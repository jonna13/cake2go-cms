'use strict';

import baseConfig from './base';

let config = {
  appEnv: 'dist',  // feel free to remove the appEnv property here
  PROJECT_PATH: 'http://midas.appsolutely.ph/',
  PROJECT_BASE: '/dashboard/',
};

export default Object.freeze(Object.assign({}, baseConfig, config));
