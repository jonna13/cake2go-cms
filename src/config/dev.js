'use strict';

import baseConfig from './base';

let config = {
  appEnv: 'dev',  // feel free to remove the appEnv property here
  // PROJECT_PATH: 'http://13.228.225.86/',
  // PROJECT_PATH: 'http://henannrewards.com/',
  PROJECT_PATH: 'https://cake2go.com.ph/',
  PROJECT_BASE: 'dashboard',
};

export default Object.freeze(Object.assign({}, baseConfig, config));
