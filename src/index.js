import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './stores';

// import { createHistory, useBasename } from 'history';
// import { Router, useRouterHistory } from 'react-router'
import { Router, hashHistory } from 'react-router';
import Config from './config/base';
import Routes from './containers/Route';

import injectTapEventPlugin from 'react-tap-event-plugin';
import { syncHistoryWithStore } from 'react-router-redux';

const store = configureStore();

injectTapEventPlugin();

// const browserHistory = useBasename(createHistory)({
//     basename: Config.PROJECT_BASE
// });
// const browserHistory = useRouterHistory(createHistory)({
//   basename: Config.PROJECT_BASE
// });

const history = syncHistoryWithStore(hashHistory, store);

render(
  <Provider store={store}>
    <Router history={history} routes={Routes} />
  </Provider>,
  document.getElementById('app')
);
