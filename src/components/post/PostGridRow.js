import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Checkbox from 'material-ui/Checkbox';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import TextField from 'material-ui/TextField';
import AppStyles from '../../styles/Style';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';


class PostGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <MoreVertIcon />
          </IconButton>
        </td>
        <td>
          <label className='capital'>{item.status}</label>
        </td>
        <td className="pointer blue-text" onClick={()=> { this.props.onEditClick(item); }}>
          {item.title}
        </td>
        <td style={{ width: '35%' }}>
          <FormGroup className="pull-left" style={{ position: 'absolute' }}>
            <Label className="label-dft strong-6 title-gotham pull-left"> Start Date:</Label>
            <TextField
              name="start"
              type="datetime-local"
              className='table-label-datetime'
              defaultValue={item.start}
              readOnly = "true"
            />
          </FormGroup>
          <FormGroup >
            <Label className="label-dft strong-6 title-gotham pull-left"> End Date:</Label>
            <TextField
              name="end"
              type="datetime-local"
              className='table-label-datetime'
              defaultValue={item.end}
              readOnly = "true"
            />
          </FormGroup>
        </td>
      </tr>
    );
  }
}

export default PostGridRow;
