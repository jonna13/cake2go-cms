/**
 * Created by jedachas on 3/30/17.
 */
'use strict';

import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import {CleanWord} from '../Utility';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Config from '../../../config/base';


class DefaultTextArea extends Component {

  state = {
    remaining: 0
  }

  handleChange = (e) => {
    this.props.onChange(e);
    this.setState({text: e.target.value });
    let chars = e.target.value;
    this.setState({remaining: this.props.maxSize - chars.length })

    const {dialogActions} = this.props;
    console.log();
    if (this.state.remaining <= 0) {
      this.props.dialogActions.openNotification('Oops! Only 1500 characters allowed.',
        Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
  }

  componentWillMount(){
    this.setState({ remaining: (this.props.defaultValue.length > 0) ?
      (this.props.maxSize - this.props.defaultValue.length) : this.props.maxSize });
  }

  render() {
    let areaHolderClass = classNames({
      'text-area-holder-default': (!this.props.wide && !this.props.regular),
      'text-area-holder-wide': this.props.wide,
      'text-area-holder-regular': this.props.regular
    });
    let textareaClass = classNames({
      'text-area-content-wide': this.props.wide,
      'text-area-content-regular': this.props.regular
    });

    return (
      <div className={areaHolderClass}>
        <div disabled={( this.state.remaining == '1500' ) ? 'true' : 'false'}>
          <TextField
            name={this.props.name}
            hintText={this.props.hintText}
            floatingLabelText={this.props.floatingLabelText}
            defaultValue={CleanWord(this.props.defaultValue)}
            onChange={this.handleChange}
            multiLine={true}
            rows={this.props.rows}
            className={textareaClass}
            fullWidth={this.props.fullWidth}
            maxLength="1500"
          /><br />
        </div>

        <span className="text-remaining">
          Remaining: {this.state.remaining}
        </span>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const props = { dialogState: state.dialogState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { dialogAction: require('../../../actions/dialogAction.js') };
  const actionMap = {dialogActions: bindActionCreators(actions.dialogAction, dispatch)};
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultTextArea);
