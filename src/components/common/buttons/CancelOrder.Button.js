'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class CancelOrderButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Cancel Order"
            onClick={this.props.handleClick}
            className="red-btn"
          />
        </div>
      </div>
    );
  }
}

export default CancelOrderButton;
