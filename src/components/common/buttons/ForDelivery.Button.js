'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class ForDeliveryButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="For Delivery"
            onClick={this.props.handleClick}
            className="red-btn"
          />
        </div>
      </div>
    );
  }
}

export default ForDeliveryButton;
