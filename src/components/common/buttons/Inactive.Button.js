'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';
import Clear from 'material-ui/svg-icons/content/clear';

class InactiveButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Inactive"
            onClick={this.props.handleOpen}
            className="white-btn"
            icon={<Clear className="red-icon" />}
          />
        </div>
      </div>
    );
  }
}

export default InactiveButton;
