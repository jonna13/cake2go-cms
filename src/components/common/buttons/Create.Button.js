'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class CreateButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Create"
            onClick={this.props.handleOpen}
            className="red-btn"
          />
        </div>
      </div>
    );
  }
}

export default CreateButton;
