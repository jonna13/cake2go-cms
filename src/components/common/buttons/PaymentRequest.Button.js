'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class PaymentRequestButton extends React.Component{
  render(){
    return(
      <div>
        <div>
          <RaisedButton
            label="Send Payment Request"
            onClick={this.props.handleClick}
            className="red-btn"
          />
        </div>
      </div>
    );
  }
}

export default PaymentRequestButton;
