'use strict';

import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class AddButton extends React.Component{
  render(){
    return(
      <div>
        {/*<div className='v-floating-action-circle hidden-xs'>
          <IconButton tooltip="Click to add" touch={true} tooltipPosition="bottom-left" onClick={this.props.handleOpen}>
            <Add className='floating-button-icon'/>
          </IconButton>
        </div>

        <div className='floating-button visible-xs'>
          <IconButton touch={true} tooltipPosition="bottom-left" onClick={this.props.handleOpen}>
            <Add className='floating-button-icon'/>
          </IconButton>
        </div>*/}
        <div>
          <RaisedButton
            label="Create New"
            onClick={this.props.handleOpen}
            className="blue-btn"
          />
        </div>
      </div>
    );
  }
}

export default AddButton;
