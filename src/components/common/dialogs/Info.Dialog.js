'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {Table} from 'react-bootstrap';
import _ from 'lodash';

class InfoDialog extends Component{
  state = {
    open: false
  }

  componentDidMount() {
    this.props.onRef(this);
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
  }

  fetchData = (id) => {
    const {actions} = this.props;
    actions.viewSingleProduct(id, this.props.identity);
    this.setState({open:true});
  }

  handleClose = () => {
    const {actions} = this.props;
    actions.removeProductIncludeSuccess();
    this.setState({open:false});
  }

  render() {
    const actions = [
      <FlatButton
        label="Ok"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleClose}
      />,
    ];

    return (
      <div>
        <Dialog
          title="Product Information"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.props.onClose}
        >
          { (!_.isEmpty(this.props.productState.selectedInclude)) ?
            <Table striped hover className='list-table'>
                <thead>
                  <tr>
                    <th>Product Code</th>
                    <th>Name</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  { this.props.productState.selectedInclude.map((x, idx) => {
                    return(
                      <tr key={idx}>
                        <td>{x.productCode}</td>
                        <td>{x.name}</td>
                        <td>{x.price}</td>
                      </tr>
                    );
                  }) }
                </tbody>
            </Table> : <div>No data</div>
           }
        </Dialog>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const props = { productState: state.productState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { productAction : require('../../../actions/productAction.js') };
  const actionMap = { actions: bindActionCreators(actions.productAction, dispatch) };
  return actionMap;
}


InfoDialog.defaultProps = {
  open: false
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoDialog);
