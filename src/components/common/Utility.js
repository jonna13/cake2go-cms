export let EmailIsValid = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export let CleanWord = (word) => {
  return word.replace(/<br\s*\/?>/mg,"\n");
}

export let ProperCase = (str) => {
  return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

export let GeneratePassword = () => {
  var Expression = (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{5,}$/);
  var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
    length = 8;
  var result = '';

  do{
    result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];

  }while( !result.match(Expression) );

  return result;
}

export let parseDate = (a) => {
  "use strict";
    var input, monthNames, day, month, year;
    if(a == ''){
      return '';
    }else if(a == '0000-00-00'){
      return '';
    }else if(a == 'NaN-NaN-NaN'){
      return '';
    }else if(a == undefined){
      return '';
    }else {
      //we can chain the methods for "input" variable:
      input = a.split(" ")[0].split("-");
      day = input[2];
      month = input[1];
      year = input[0];
      monthNames = "January,February,March,April,May,June,July,August,September,October,November,December";
      monthNames = monthNames.split(",");
      return monthNames[Number(month) - 1] + " " + day + ", " + year;
    }
}
