'use strict';

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ThumbImage from './table/ThumbImage';
import Config from '../../config/base';
import Info from 'material-ui/svg-icons/action/info';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import styles from '../../styles/Style';
const defaultImage = require('../../images/no-image.jpg');

class MultipleImageUpload extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: ''
    }
  }

  _handleImageChange(e) {
    e.preventDefault();

    let files = e.target.files;

    for (var i = 0; i < files.length; i++) {
      let file = files[i];

      let reader = new FileReader();
      console.log('file', file);
      console.log('reader', reader);

      reader.onloadend = () => {
        if (file.size > Config.IMAGE_SIZE_LIMIT) {
          this.props.dialogActions.openNotification('Image size is too large',
            Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
            return;
        }
        if (file.type > Config.IMAGE_FORMAT) {
          this.props.dialogActions.openNotification('Invalid image type',
            Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
            return;
        }
        if(files.length > 5) {
          this.props.dialogActions.openNotification('Maximum of 5 images only',
            Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
            return;
        }if(this.state.file.length > 5) {
          this.props.dialogActions.openNotification('Maximum of 5 images only',
            Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
            return;
        }

        this.setState({
          file: files,
          imagePreviewUrl: [...this.state.imagePreviewUrl, reader.result]
        });
        this.props.onImageChange(this.state);
      }
      reader.readAsDataURL(file);

    }
    console.log('files', files);
  }
  handleClear = () => {
    this.setState({
      file: '',
      imagePreviewUrl: ''
    });
  }
  render(){
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH + '' + this.props.imageModule + '/thumb/' + this.props.image;
    console.log('samplestate',imagePreviewUrl);
    let imageinfo =  (this.props.info) ? this.props.info : 'Best size: Less than 100kb. Max 2MB';
    let imagesPrev = [];
    if (imagePreviewUrl.length > 0) {
      imagePreviewUrl.forEach((val, key) => {
        imagesPrev.push(
          <img key={key} className="img-cover" src={val}/>
        );
      })
    }
    else{
      if (this.props.image) {
        $imagePreview = (<ThumbImage image={path} />);
      }
      else{
        $imagePreview = (<ThumbImage image={defaultImage} />);
      }
    }
    return(
      <div>
        <div className="modal-image-preview">
          <div className="images-array-grid">
          { imagesPrev }
          </div>
        </div>
        <div className="modal-image-info">
          <Info/><div>{imageinfo}</div>
        </div>
        <div className="modal-image-button">
        {(imagePreviewUrl.length > 0)?
          <RaisedButton className="red-text-btn" secondary={true} label="Change" onClick={this.handleClear}/>
          :
           <RaisedButton className="red-text-btn" primary={true} containerElement='image' label="Choose an Image" labelPosition="before" onChange={(e)=>this._handleImageChange(e)}>
              <input type="file" style={styles.imageInput} multiple/>
            </RaisedButton>

          }
        </div>
        {/*<div className="modal-image-preview">
          { $imagePreview }
        </div>
        <div className="modal-image-info">
          <Info/><div>{imageinfo}</div>
        </div>
        <div className="modal-image-button">
           <FlatButton primary={true} containerElement='image' label="Choose an Image" labelPosition="before" onChange={(e)=>this._handleImageChange(e)}>
              <input type="file" style={styles.imageInput} multiple/>
            </FlatButton>
        </div>*/}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const props = { dialogState: state.dialogState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { dialogAction: require('../../actions/dialogAction.js') };
  const actionMap = {dialogActions: bindActionCreators(actions.dialogAction, dispatch)};
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(MultipleImageUpload);
