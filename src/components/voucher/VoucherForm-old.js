/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import TextField from 'material-ui/TextField';
import { DefaultTextArea } from '../common/fields';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';


class VoucherForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      bday: '',
      type: '',
      shouldDisplay: false,
      startDate: null,
      endDate: null,
      hasDate: ['flash', 'wemissyou']
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        if (nextProps.data.selectedRecord.type == 'birthday'){
          let date = new Date(nextProps.data.selectedRecord.startDate);
          this.setState({bday:date.getMonth()});
        }
        this.setState({
          type: nextProps.data.selectedRecord.type,
          shouldDisplay: true,
          startDate: new Date(nextProps.data.selectedRecord.startDate),
          endDate: new Date(nextProps.data.selectedRecord.endDate),
        });
      }
    }
  }

  handleTypeChange = (e, idx, type) => {
    this.setState({type});
    this.props.onTypeChange(type);
  }

  handleBirthdayChange = (e, idx, bday) => {
    this.setState({bday});
    this.props.onBdayChange(bday);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    const {data} = this.props;
    let startNoDefault = true,
      endNoDefault = true;

    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December'];
    let monthList = []
    monthList.push(<MenuItem key={0} value='' primaryText='Select Month' />);
    months.map( (val, idx) => {
      monthList.push(<MenuItem key={(idx+1)} value={idx} primaryText={val} />);
    });

    if (this.state.shouldDisplay){

      startNoDefault = (this.props.shouldEdit) ? ((data.selectedRecord.startDate == '0000-00-00') ? true : false)
        : true;
      endNoDefault = (this.props.shouldEdit) ? ((data.selectedRecord.endDate == '0000-00-00') ? true : false)
        : true;
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}
                    info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <DefaultTextArea
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  rows={2}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  disabled={(this.props.shouldEdit) ? true:false}
                  /><br />
                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <DefaultTextArea
                  name="terms"
                  hintText="Enter Terms"
                  floatingLabelText="Terms"
                  defaultValue={(data.selectedRecord.terms) ? data.selectedRecord.terms : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <DropDownMenu value={this.state.type} onChange={this.handleTypeChange}
                  autoWidth={false} className='dropdownButton'>
                  <MenuItem value="" primaryText="Select Type" />
                  <MenuItem value="flash" primaryText="Flash" />
                  <MenuItem value="birthday" primaryText="Birthday" />
                  <MenuItem value="wemissyou" primaryText="We Miss You" />
                  <MenuItem value="registration" primaryText="Registration" />
                  <MenuItem value="membership" primaryText="Membership" />
                </DropDownMenu>
                <TextField
                  name="wmyperiod"
                  type="number"
                  hintText="Enter We Miss You Period"
                  floatingLabelText="We Miss You Period"
                  defaultValue={(data.selectedRecord.wmyperiod) ? data.selectedRecord.wmyperiod : '' }
                  onChange={this.props.onChange}
                  className={(this.state.type == 'wemissyou') ? 'textfield-default' : 'hide'}
                />
                <DropDownMenu
                  value={this.state.bday}
                  onChange={this.handleBirthdayChange}
                  autoWidth={true}
                  className={(this.state.type == 'birthday') ? 'dropdownButton' : 'hide'}>
                  {monthList}
                </DropDownMenu><br />

                { startNoDefault ?
                  <DatePicker
                    hintText="Enter Start Date"
                    floatingLabelText="Start Date"
                    onChange={this.props.onStartDate}
                    firstDayOfWeek={0}
                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                    /> :
                  <DatePicker
                    hintText="Enter Start Date"
                    floatingLabelText="Start Date"
                    onChange={this.props.onStartDate}
                    defaultDate={this.state.startDate}
                    firstDayOfWeek={0}
                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                    /> }

                { endNoDefault ?
                  <DatePicker
                    hintText="Enter End Date"
                    floatingLabelText="End Date"
                    onChange={this.props.onEndDate}
                    firstDayOfWeek={0}
                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                    /> :
                  <DatePicker
                    hintText="Enter End Date"
                    floatingLabelText="End Date"
                    onChange={this.props.onEndDate}
                    defaultDate={this.state.endDate}
                    firstDayOfWeek={0}
                    className={( this.state.hasDate.indexOf(this.state.type) >= 0 ) ? 'datefield-default' : 'hide'}
                    /> }

                <TextField
                  name="quantity"
                  type="number"
                  hintText="Enter Quantity"
                  floatingLabelText="Quantity"
                  defaultValue={(data.selectedRecord.quantity) ? data.selectedRecord.quantity : '' }
                  onChange={this.props.onChange}
                  className="textfield-default"
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default VoucherForm;
