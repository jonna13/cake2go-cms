/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import { parseDate } from '../common/Utility';
import OrderDetails from './OrderDetails';
import TransActivity from './TransActivity';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';

import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';

class OrderForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      dispatcherName: '',
      startDate: moment()
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleTypeChange = (e, idx, type) => {
    this.setState({type});
    this.props.onTypeChange(type);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  handleChangeRider = (e) => {
    this.setState({
      dispatcherName: e.target.value
    });
    this.props.dispatcherName(e.target.value);
  }
  handleChangeDate = (date) => {
    this.setState({
      startDate: date
    });
    this.props.dateDelivered(date);
  }
  render(){
    const {data, paymentStatus} = this.props;
    const { dispatcherName } = this.state;
    let head = {};
    let products = [];
    let addons = [];
    let transactionActivity = [];
    if (data.selectedRecord.length > 0) {
      data.selectedRecord.forEach((val, key) => {
        if(val.partition == 'head'){
        head = val;
      }else if (val.partition == 'products') {
        products.push(val);
      }else {
        addons.push(val);
      }
      });
    }
    // console.log('data', head, products, addons);
    let date = (head.transactionDate)? head.transactionDate : '';
    let dateOnly = date.split(" ")[0];
    console.log('transDate', dateOnly);
    let newTransdate = new Date(dateOnly);
    let newDate = newTransdate.getFullYear()+'-'+(newTransdate.getMonth() + 1)+'-'+newTransdate.getDate();
    let address = head.unit_floor_houseno+' '+head.street+', '+head.barangay_subdivision_village+' '+head.city;
    // parseDate();

    switch (paymentStatus) {
      case 'New Order':
        transactionActivity.push(
          <OrderDetails
            paymentStatus={paymentStatus}
            key={0}
            products={products}
            addons={addons}
            head={head}
          />
        );
        break;
      case 'Acknowledged Order':
        transactionActivity.push(
          <TransActivity
            paymentStatus={paymentStatus}
            key={0}
            products={products}
            addons={addons}
            head={head}
          />
        );
        break;
      case 'Proceed to Payment':
        transactionActivity.push(
          <TransActivity
            paymentStatus={paymentStatus}
            key={0}
            products={products}
            addons={addons}
            head={head}
          />
        );
        break;
      case 'Payment Settled':
        transactionActivity.push(
          <OrderDetails
            paymentStatus={paymentStatus}
            key={0}
            products={products}
            addons={addons}
            head={head}
          />
        );
        break;
      case 'Processing':
        transactionActivity.push(
          <TransActivity
            paymentStatus={paymentStatus}
            key={0}
            products={products}
            addons={addons}
            head={head}
          />
        );
        break;
      case 'In Transit':
        transactionActivity.push(
          <TransActivity
            paymentStatus={paymentStatus}
            key={0}
            products={products}
            addons={addons}
            head={head}
          />
        );
        break;
      case 'Delivered':
        transactionActivity.push(
          <TransActivity
            paymentStatus={paymentStatus}
            key={0}
            products={products}
            addons={addons}
            head={head}
          />
        );
        break;
      default:

    }


    if (this.state.shouldDisplay){
      let datetime = parseDate(newDate);
      console.log('sampledate', datetime, newDate);
      return(
        <Grid fluid>
          {(paymentStatus == 'Processing')?
            <div className="order-rider-name-grid">
              <h5 className="order-details">Rider Name:</h5>
              <FormGroup>
                <Input type="text" name="search" id="- Search By Name -" placeholder="" value={dispatcherName}  className="fullwidth" onChange={this.handleChangeRider}/>
                {(this.props.validate)?
                  <p style={{color: '#b9302c', margin: '0'}}>This field is required.</p>
                :
                  ''
                }
              </FormGroup>
              <div className="spacer"/>
            </div>
          :
            ''
          }

          {(paymentStatus == 'In Transit')?
            <div className="order-rider-name-grid">
              <h5 className="order-details">Date and Time Delivered:</h5>
              <DatePicker
                minDate={moment()}
                selected={this.state.startDate}
                onChange={this.handleChangeDate}
              />
              <div className="spacer"/>
            </div>
          :
            ''
          }
          <div className="order-detail-main">
            <h5 className="order-details">Transaction Number: <span>{head.transactionID}</span></h5>
            {(paymentStatus == 'Payment Settled')?
              <h5 className="order-details">Paypal Transaction Number: <span style={{color: '#b9302c'}}>{head.paypalTransID}</span></h5>
            :
              ''
            }
            <h5 className="order-details">Delivery Date and Time: <span>{datetime}</span></h5>
            <h5 className="order-details">Location: <span>{head.locName}</span></h5>
          </div>
          <h3 style={{fontWeight: '300', color: '#000'}}>Recipient Details</h3>
          <div className="order-detail-main-grid">
            <h5 className="order-details">First Name: <span>{head.fname}</span></h5>
            <h5 className="order-details">Last Name: <span>{head.lname}</span></h5>
            <h5 className="order-details">Telephone: <span>{head.landlineNum}</span></h5>
            <h5 className="order-details">Mobile: <span>{head.mobileNum}</span></h5>
            <h5 className="order-details">Gender: <span>{head.gender}</span></h5>
            <h5 className="order-details">Email: <span>{head.email}</span></h5>
            <h5 className="order-details">Address: <span>{address}</span></h5>
          </div>
          {(paymentStatus == 'New Order')?
            <div className="order-product-grid" style={{borderBottom: '1px solid #555', marginBottom: '0'}}>
              <div className="spacer"/>
              <h5>Order Details</h5>
              <h5 style={{justifySelf: 'center'}}>Quantity</h5>
              <h5 style={{justifySelf: 'center'}}>Price</h5>
            </div>
          :
            ''
          }
          { transactionActivity }
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default OrderForm;
