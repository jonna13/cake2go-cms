'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import Search from 'material-ui/svg-icons/action/search';
import Config from '../../config/base';
import {AddButton, ViewButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';
import jwtDecode from 'jwt-decode';
import OrderList from './OrderList';

class OrderComponent extends React.Component {
  state = {
    filter: '',
    value: ''
  }
  componentWillMount() {
    let loc = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    let locIDparam;
    let locRole;
    if(loc.locID == 'main'){
      locIDparam = '';
    }else {
      locIDparam = loc.locID;
    }

    let {pageinfo} = this.props.data.orders;
    let {params} = this.props;
    console.log('sample params', params.paymentStatus);
    pageinfo.selectedLocation = locIDparam;
    pageinfo.selectedStatus = (params.paymentStatus == undefined)? '' : params.paymentStatus;
    this.handleFilterQuery();
  }
  componentWillReceiveProps(e){
    // let {pageinfo} = e.data.orders;
    // let {params} = e;
    // pageinfo.selectedStatus = params.paymentStatus;
    // this.handleFilterQuery();
  }
  handleAdd = () => {
    const {router} = this.props;
    router.push('/order_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/orders/'+item.transactionID+'/'+item.paymentStatus);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.orders;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.orders;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (e) => {
    this.setState({
      search: e.target.value
    });
    let {pageinfo} = this.props.data.orders;
    pageinfo.searchFilter = e.target.value;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.orders;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }
  handleSearchFilter = (e) => {
    this.setState({
      filter: e.target.value
    });
    let {pageinfo} = this.props.data.orders;
    pageinfo.selectedType = e.target.value;
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.orders;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus,
      selectedLocation: pageinfo.selectedLocation,
      selectedType: pageinfo.selectedType
    }
    actions.getOrders(params);
  }
  handleSelectPaymentStatus = (e) => {
    this.setState({search: '', filter: ''});
    let {pageinfo} = this.props.data.orders;
    const { router } = this.props;
    pageinfo.selectedStatus = e;
    router.push('/orders/'+e);
    this.handleFilterQuery();
  }

  render() {
    const {pageinfo} = this.props.data.orders;
    const {data} = this.props;
    const {filter, search} = this.state;
    let loc = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME));
    return (
      <div>
        <h2 className='content-heading title-gotham'>All Orders</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ (pageinfo == undefined)? '' : pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
          <CheckStatusBar
            pageinfo={data.orders.pageinfo}
            handleChange={this.handleStatusChange}/>
          <PageSizeComponent
            pageinfo={data.orders.pageinfo}
            handleChange={this.handlePageSize}/>
          <SearchBar
            pageinfo={data.orders.pageinfo}
            handleChange={this.handleSearchChange}
        handleClose={this.handleSearchClose}/>*/}

        <Card className="order-card">
          <Form>
            <Row>
              <Col md={2}>
                <Label className="label-dft strong-6 title-gotham">Check Order Status </Label>
              </Col>
              <Col md={4}>
                <Input type="select" value={filter} name="select" id="exampleSelect" className="fullwidth" onChange={this.handleSearchFilter}>
                  <option value="">- Select Filter -</option>
                  <option value="name">- Search By Name -</option>
                  <option value="transactionID">- Search By Trans. No. -</option>
                  <option value="locName">- Search By Branch -</option>
                </Input>
              </Col>
              <Col md={5}>
                <Label hidden>Search</Label>
                <Input type="text" name="search" value={search} disabled={filter? false : true} onChange={this.handleSearchChange} id="- Search By Name -" placeholder=""  className="fullwidth"/>
              </Col>
              <Col md={1}>
                <Label className="search"><Search className='nav-icon'/></Label>
              </Col>
            </Row>
            {/* <Row>
              <Col md={2}>
                <Label className="label-dft strong-6 title-gotham">Status Result<span className="pull-colo-r">:</span> </Label>
              </Col>
              <Col md={4}>
                <p className="label-dft"> Branch - <span className="label-dft green-txt"> Preparing</span></p>

              </Col>
              <Col md={5}>

                <FormGroup className="pull-right-responsive">
              <ViewButton
              handleOpen={this.handleAdd}
              style= {{ float: 'right'}}/>
                </FormGroup>
              </Col>
              <Col md={1}>
                <FormGroup>
                </FormGroup>
              </Col>
            </Row> */}
          </Form>
        </Card>
        <br/>

        { /* List  */ }
        <div className="sub-heading ">Orders</div>
        <div className="view-orders-grid">
          <h4 style={{margin: '0 10px 0 0'}}>View:</h4>
          {(loc.role != 'branch')?
            <FlatButton label="All" style={{color: ((pageinfo.selectedStatus == '')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('')}}/>
          :
            ''
          }
          {(loc.role != 'branch')?
            <div className="filter-btn">
              <FlatButton label="NEW" style={{color: ((pageinfo.selectedStatus == 'New Order')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('New Order')}}/>
            </div>
          :
            ''
          }
          {(loc.role != 'branch')?
            <div className="filter-btn">
              <FlatButton label="ACKNOWLEDGE" style={{color: ((pageinfo.selectedStatus == 'Acknowledged Order')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('Acknowledged Order')}}/>
            </div>
          :
            ''
          }
          {(loc.role != 'branch')?
            <div className="filter-btn">
              <FlatButton label="FOR PAYMENT" style={{color: ((pageinfo.selectedStatus == 'Proceed to Payment')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('Proceed to Payment')}}/>
            </div>
          :
            ''
          }

          <div className="filter-btn">
            <FlatButton label="PAID" style={{color: ((pageinfo.selectedStatus == 'Payment Settled')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('Payment Settled')}}/>
          </div>
          <div className="filter-btn">
            <FlatButton label="PROCESSING" style={{color: ((pageinfo.selectedStatus == 'Processing')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('Processing')}}/>
          </div>
          <div className="filter-btn">
            <FlatButton label="IN TRANSIT" style={{color: ((pageinfo.selectedStatus == 'In Transit')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('In Transit')}}/>
          </div>
          <div className="filter-btn">
            <FlatButton label="DELIVERED" style={{color: ((pageinfo.selectedStatus == 'Delivered')? '#c11d0f' : '#337ab7')}} onClick={() => {this.handleSelectPaymentStatus('Delivered')}}/>
          </div>
          <div className="spacer"/>
        </div>
        {/* <div><b>View:</b> <a>ALL</a> | <a>NEW</a> | <a>ACKNOWLEDGED</a> | <a>PAID</a></div> */}
        <OrderList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default OrderComponent;
