import React from 'react';
import defaultImg from '../../images/LOGO-hq.png';
import Config from '../../config/base';
import { parseDate } from '../common/Utility';
import RaisedButton from 'material-ui/RaisedButton';

class TransActivity extends React.Component{
  render(){
    const { head, paymentStatus } = this.props;
    let date = parseDate(head.dateModified);
    return(
      <div className="transaction-activity-main">
        <h3>Transaction Activity</h3>
        <div className="transaction-activity-grid">
          <h4>Order Placed</h4>
          <h4>{date}</h4>
          <div className="spacer"/>
        </div>
        <div className="transaction-activity-grid">
          <h4>Acknowledged Order</h4>
          <h4>{date}</h4>
          <div className="spacer"/>
        </div>
        {(paymentStatus == 'Proceed to Payment' || paymentStatus == 'Processing')?
          <div className="transaction-activity-grid">
            <h4>Email Sent</h4>
            <h4>{date}</h4>
            {(paymentStatus == 'Processing')?
              <div className="spacer"/>
            :
            <RaisedButton
              label="Resend Payment Request"
              className="blue-btn"
              onClick={this.props.resendPaymentRequest}
            />
            }
          </div>
        :
            ''
        }

        {(paymentStatus == 'Processing' || paymentStatus == 'In Transit' || paymentStatus == 'Delivered')?
          <div className="transaction-activity-grid">
            <div>
              <h4 style={{marginBottom: '10px'}}>Order paid</h4>
              <h5>Paypal ID: {head.email}</h5>
              <h5>Amount: ₱{head.grandTotal}</h5>
            </div>
            <h4 style={{alignSelf: 'start'}}>{date}</h4>
            <div className="spacer"/>
          </div>
        :
          ''
        }
        {(paymentStatus == 'In Transit' || paymentStatus == 'Delivered')?
          <div className="transaction-activity-grid">
            <div>
              <h4 style={{marginBottom: '10px'}}>In Transit</h4>
              <h5>Rider Name: {head.dispatcherName}</h5>
            </div>
            <h4 style={{alignSelf: 'start'}}>{date}</h4>
            <div className="spacer"/>
          </div>
        :
          ''
        }
        {(paymentStatus == 'Delivered')?
          <div className="transaction-activity-grid">
            <h4>Delivered</h4>
            <h4>{date}</h4>
            <div className="spacer"/>
          </div>
        :
          ''
        }
      </div>
    );
  }
}
export default TransActivity;
