import React from 'react';
import defaultImg from '../../images/LOGO-hq.png';
import Config from '../../config/base';

class OrderDetails extends React.Component{
  render(){
    const { products, addons, head, paymentStatus } = this.props;
    console.log('prod', products);
    let prodList = [];
    if (products.length > 0) {
      products.forEach((val, key) => {
        let addonList = [];
        // let imgsrc = (val.image[0] != undefined)? val.image[0] : defaultImg;
        let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH + '' + Config.IMAGE.PRODUCT + '/full/' + val.image;
        prodList.push(
          <div key={key} style={{borderBottom: '1px dotted #2d5988', marginBottom: '20px'}}>
            <div className="order-product-grid">
              <div>
                <div className="img-cover" style={{backgroundImage: 'url('+(path)+')'}}/>
              </div>
              <div>
                <h4>{val.productName}</h4>
                <p>{val.productDescription}</p>
              </div>
              <h3 style={{justifySelf: 'center'}}>{val.prodQty}</h3>
              <h3 style={{justifySelf: 'end'}}>₱{val.amount}</h3>
              <div className="spacer"/>
              <h4 style={{margin: '0'}}>Addons:</h4>
              <div className="spacer"/>
              <div className="spacer"/>
            </div>
            { addonList }
          </div>
        );
          if (addons.length > 0) {
            addons.forEach((value, keys) => {
              if(val.cartID == value.cartID){
                addonList.push(
                  <div key={keys} className="order-product-grid">
                    <div className="spacer"/>
                    <div>
                      <h4>{value.productName}</h4>
                      {
                        (value.addonID == 'Addons001')?
                          <div>
                            <h5>
                              {(value.addon_to)? ('To: '+value.addon_to) : ''}
                            </h5>
                            <h5>
                              {(value.addon_from)? ('From: '+value.addon_from) : ''}
                            </h5>
                          </div>
                        :
                        ''
                      }
                      {
                        ((value.addonID == 'Addons001') || (value.addon_type == 'Addons002'))?
                          <h5>
                            {(value.addon_note)? ('Message: '+value.addon_note) : ''}
                          </h5>
                        :
                        ''
                      }
                      {
                        (value.addonID == 'Addons003')?
                          <h5>
                            {(value.addon_color)? ('Color: '+value.addon_color) : ''}
                          </h5>
                        :
                        ''
                      }
                    </div>
                    <h3 style={{justifySelf: 'center'}}>{value.addonQty}</h3>
                    <h3 style={{justifySelf: 'end'}}>₱{value.addonPrice}</h3>
                  </div>
                );
              }
            });
          }

      });
    }

    return(
      <div className="order-product-main">
        <div className="order-product-list-container">
          { prodList }
        </div>
        <div className="order-product-list-total" style={{backgroundColor: ((paymentStatus == 'Payment Settled')? '#b9b9b9' : '')}}>
          <h3>TOTAL AMOUNT</h3>
          <h3 style={{justifySelf: 'end'}}>₱{head.grandTotal}</h3>
        </div>
      </div>
    );
  }
}
export default OrderDetails;
