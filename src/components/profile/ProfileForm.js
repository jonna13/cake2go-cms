/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import {CleanWord} from '../common/Utility';

class ProductForm extends React.Component{

  state = {
    shouldDisplay: false,
  };

  componentWillMount(){
    if (!_.isEmpty(this.props.data.records)){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.records)) {
        this.setState({shouldDisplay: true});
      }
    }
  }


  render(){
    const {data} = this.props;
    console.info('ProfileForm', data);
    console.info('shouldDisplay', this.state.shouldDisplay);

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.records.profilePic) ? data.records.profilePic : '' }
                    imageModule={this.props.imageModule}
                    info="Ratio 2 : 1 ( ex: 1000x500 pixels )"/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="company"
                  type="text"
                  hintText="Enter Company"
                  floatingLabelText="Company"
                  defaultValue={(data.records.company) ? data.records.company : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="fname1"
                  hintText="Enter First Name"
                  floatingLabelText="First Name (Primary Contact)"
                  defaultValue={(data.records.fname1) ? data.records.fname1 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="mname1"
                  hintText="Enter Middle Name"
                  floatingLabelText="Middle Name (Primary Contact)"
                  defaultValue={(data.records.mname1) ? data.records.mname1 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="lname1"
                  hintText="Enter Last Name"
                  floatingLabelText="Last Name (Primary Contact)"
                  defaultValue={(data.records.lname1) ? data.records.lname1 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="fname2"
                  hintText="Enter First Name"
                  floatingLabelText="First Name (Secondary Contact)"
                  defaultValue={(data.records.fname2) ? data.records.fname2 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="mname2"
                  hintText="Enter Middle Name"
                  floatingLabelText="Middle Name (Secondary Contact)"
                  defaultValue={(data.records.mname2) ? data.records.mname2 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="lname2"
                  hintText="Enter Last Name"
                  floatingLabelText="Last Name (Secondary Contact)"
                  defaultValue={(data.records.lname2) ? data.records.lname2 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="landline1"
                  hintText="Enter Landline"
                  floatingLabelText="Primary Landline Number"
                  defaultValue={(data.records.landline1) ? data.records.landline1 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="landline2"
                  hintText="Landline"
                  floatingLabelText="Secondary Landline Number"
                  defaultValue={(data.records.landline2) ? data.records.landline2 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="mobile1"
                  hintText="Enter Mobile"
                  floatingLabelText="Primary Mobile Number"
                  defaultValue={(data.records.mobile1) ? data.records.mobile1 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="mobile2"
                  hintText="Enter Mobile"
                  floatingLabelText="Secondary Mobile Number"
                  defaultValue={(data.records.mobile2) ? data.records.mobile2 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="fax1"
                  hintText="Enter Fax"
                  floatingLabelText="Primary Fax"
                  defaultValue={(data.records.fax1) ? data.records.fax1 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="fax2"
                  hintText="Enter Fax"
                  floatingLabelText="Secondary Fax"
                  defaultValue={(data.records.fax2) ? data.records.fax2 : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="email"
                  hintText="Enter Email"
                  floatingLabelText="Email"
                  defaultValue={(data.records.email) ? data.records.email : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                  className="textfield-regular"
                /><br />
                <DefaultTextArea
                  name="address"
                  hintText="Enter Address"
                  floatingLabelText="Address"
                  defaultValue={(data.records.address) ? CleanWord(data.records.address) : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  className="textfield-regular"
                  /><br />
                <DefaultTextArea
                  name="about"
                  hintText="Enter About"
                  floatingLabelText="About"
                  defaultValue={(data.records.about) ? CleanWord(data.records.about) : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <TextField
                  name="website"
                  hintText="Enter Website"
                  floatingLabelText="Website"
                  defaultValue={(data.records.website) ? data.records.website : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="merchantCode"
                  hintText="Enter Merchant Code"
                  floatingLabelText="Merchant Code"
                  defaultValue={(data.records.merchantCode) ? data.records.merchantCode : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default ProductForm;
