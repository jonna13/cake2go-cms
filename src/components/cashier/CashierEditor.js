/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import {postUrl} from '../../api/data.api';
import _ from 'lodash';
import PrintComponent from '../common/PrintComponent';

import CashierForm from './CashierForm';

class CashierEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      cashier: {
        name: '',
        description: '',
        image: '',
        status: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.CASHIER,
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewCashier(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.cashierState.selectedRecord)) {
        this.setState({
          cashier: nextProps.cashierState.selectedRecord
        });
      }
    }
  }

  // handle going back to Cashier
  handleReturn = () => {
    this.props.router.push('/cashier');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.cashier)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_CASHIER_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateCashier(this.state.cashier, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    console.info('handleAdd:state', this.state);
    if (this.validateInput(this.state.cashier)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_CASHIER_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addCashier(this.state.cashier, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let cashier = this.state.cashier;
    cashier[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.cashier['status'] = e;
  }

  handlePrint = (e) => {
    console.info('printing...');
    window.location = Config.PROJECT_PATH + 'dashboard/php/api.qrcode.php?id='
      +this.props.cashierState.selectedRecord.cashierID;
  }

  render(){
    let print = (this.props.params.id) ? <PrintComponent onPrint={this.handlePrint}/> : null;
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Cashier' : 'Add Cashier'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }
        {print}

        { /* Form */ }
        <CashierForm
          data={this.props.cashierState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onStatusChange={this.handleStatusChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    cashierState: state.cashierState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cashierAction: require('../../actions/cashierAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.cashierAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CashierEditor);
