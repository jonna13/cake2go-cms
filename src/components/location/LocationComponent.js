'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton, ViewButton, InactiveButton, ActiveButton} from '../common/buttons';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';

import LocationList from './LocationList';

class LocationComponent extends React.Component {

  handleAdd = () => {
    const {router} = this.props;
    router.push('/branch_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/branch/'+item.locID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.locations;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.locations;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.locations;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.locations;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.locations;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    console.info('LocationComponent: param', params);
    actions.getLocations(params);
  }

  render() {
    const {pageinfo} = this.props.data.locations;
    console.log("pageinfo", pageinfo);
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Branches</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.locations.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.locations.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.locations.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

          <Card className="order-card">
            <Form>
              <Row>
                <Col md={12} className="inline-block">
                  <AddButton
                    handleOpen={this.handleAdd}/>&nbsp;&nbsp;
                </Col>
              </Row>
            </Form>
          </Card>

        { /* List */ }
        <LocationList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

LocationComponent.displayName = 'LocationLocationComponent';

// Uncomment properties you need
// LocationComponent.propTypes = {};
// LocationComponent.defaultProps = {};

export default LocationComponent;
