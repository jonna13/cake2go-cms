/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import LoyaltyForm from './LoyaltyForm';


class LoyaltyEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loyalty: {
        name: '',
        points: 0,
        promoType: '',
        categoryID: '',
        categoryName: '',
        locID: '',
        locName: '',
        terms: '',
        description: '',
        status: false,
        image: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.REWARDS
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewLoyalty(this.props.params.id);
    }
    actions.getCategorys();
    actions.getLocations();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.loyaltyState.selectedRecord)) {
        this.setState({
          loyalty: nextProps.loyaltyState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/loyalty');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInputUpdate(this.state.loyalty)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_LOYALTY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateLoyalty(this.state.loyalty, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.loyalty)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_LOYALTY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addLoyalty(this.state.loyalty, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let loyalty = this.state.loyalty;
    loyalty[field] = e.target.value;
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (_.isEmpty(this.state.uploadImage)) {
      dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.locID == '') {
      dialogActions.openNotification('Oops! Please select location', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.points == 0) {
      dialogActions.openNotification('Oops! No points found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.promoType == '') {
      dialogActions.openNotification('Oops! No type found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.description == '') {
      dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.terms == '') {
      dialogActions.openNotification('Oops! No terms found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    let newDescription = data.description.replace(/<br\/>/g, "\n");
    let newTerms = data.terms.replace(/<br\/>/g, "\n");
    this.state.loyalty['description'] = newDescription;
    this.state.loyalty['terms'] = newTerms;

    return true;
  }
    validateInputUpdate = (data) => {
      const {dialogActions} = this.props;
      if (data.name == '') {
        dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.locID == '') {
        dialogActions.openNotification('Oops! Please select location', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.points == 0) {
        dialogActions.openNotification('Oops! No points found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.promoType == '') {
        dialogActions.openNotification('Oops! No type found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.description == '') {
        dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
      if (data.terms == '') {
        dialogActions.openNotification('Oops! No terms found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }

      let newDescription = data.description.replace(/<br\/>/g, "\n");
      let newTerms = data.terms.replace(/<br\/>/g, "\n");
      this.state.loyalty['description'] = newDescription;
      this.state.loyalty['terms'] = newTerms;

      return true;
    }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.loyalty['status'] = e;
  }

  // handle level
  handleLevelChange = (e) => {
    console.info(e);
    this.state.loyalty['levelID'] = e;
  }

  // handle change in promoType
  handleTypeChange = (value) => {
    this.state.loyalty['promoType'] = value;
  }

  // handle category
  handleCategoryChange = (e, name) => {
    this.state.loyalty['categoryID'] = e;
    this.state.loyalty['categoryName'] = name;
  }

  // handle location category
  handleLocationChange = (e, name) => {
    this.state.loyalty['locID'] = e;
    this.state.loyalty['locName'] = name;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }
  render(){
    console.log('testimg', this.state.uploadImage);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Reward' : 'Add Reward'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <LoyaltyForm
          data={this.props.loyaltyState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onTypeChange={this.handleTypeChange}
          onLevelChange={this.handleLevelChange}
          onStatusChange={this.handleStatusChange}
          onCategoryChange={this.handleCategoryChange}
          onLocationChange={this.handleLocationChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    loyaltyState: state.loyaltyState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    loyaltyAction: require('../../actions/loyaltyAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.loyaltyAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LoyaltyEditor);
