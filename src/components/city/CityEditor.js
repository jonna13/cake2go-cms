/**
 * Created by jonna on 10/6/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import CityForm from './CityForm';


class CityEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      city: {
        city: '',
        status: false
      },
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewCity(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.cityState.selectedRecord)) {
        this.setState({
          city: nextProps.cityState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/deliverylocation');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.city)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_CITY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateCity(this.state.city, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.city)) {
    // if (this.validateBranchCode(this.state.city)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_CITY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addCity(this.state.city, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let city = this.state.city;
    city[field] = e.target.value;
    console.log("handleData", city[field]);
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;

    if (data.city == '') {
      dialogActions.openNotification('Oops! No city found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.city['status'] = e;
    console.log('Status', e);
  }

  // handle status select
  // handleStatusChange = (e) => {
  //   this.state.city['status'] = e;
  //   console.log('Status', e);
  // }

  //close modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    const {actions} = this.props;
    console.log('actions', actions);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit City' : 'Create New City'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? 'Edit City' : 'Create New City'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <CityForm
            data={this.props.cityState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            actions={actions}
            onStatusChange={this.handleStatusChange}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>



      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    cityState: state.cityState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    cityAction: require('../../actions/cityAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.cityAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(CityEditor);
