/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import BrandForm from './BrandForm';

class BrandEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      brand: {
        name: '',
        description: '',
        image: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.BRAND,
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewBrand(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.brandState.selectedRecord)) {
        this.setState({
          brand: nextProps.brandState.selectedRecord
        });
      }
    }
  }

  // handle going back to Brand
  handleReturn = () => {
    this.props.router.push('/brand');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.brand)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_BRAND_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateBrand(this.state.brand, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.brand)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_BRAND_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addBrand(this.state.brand, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (this.state.brand.image == '') {
        if (_.isEmpty(this.state.uploadImage)) {
          dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
          return false;
        }
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let brand = this.state.brand;
    brand[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.brand['status'] = e;
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Brand' : 'Add Brand'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <BrandForm
          data={this.props.brandState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onStatusChange={this.handleStatusChange}
          onImageChange={this.handleImageChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    brandState: state.brandState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    brandAction: require('../../actions/brandAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.brandAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(BrandEditor);
