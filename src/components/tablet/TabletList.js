/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './TabletGridRow';

class TabletList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getTablets(params);
  }

  render(){
    let {tablets} = this.props.data;
    const headers = [
      { title: '', value: 'dateAdded'},
      { title: 'Device Code', value: 'deviceCode'},
      { title: 'Device ID', value: 'deviceID'},
      { title: 'Location', value: 'locName'},
      { title: 'Location ID', value: 'locID'},
      { title: 'Status', value: 'status'},
      { title: 'Deploy Status', value: 'deploy'}
    ];

    var rows = [];
    if (tablets.records.length > 0) {
      tablets.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.tablets}
            onGetData={this.handleGetData}
            hasImage={false}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.tablets}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default TabletList;
