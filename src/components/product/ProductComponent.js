'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton, ViewButton, InactiveButton, ActiveButton, ShowAllButton} from '../common/buttons';
import SearchBar from '../common/SearchComponent';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import Search from 'material-ui/svg-icons/action/search';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Done from 'material-ui/svg-icons/action/done';
import Clear from 'material-ui/svg-icons/content/clear';


import ProductList from './ProductList';

class ProductComponent extends React.Component {

  componentWillMount() {
    let {pageinfo} = this.props.data.products;
    console.log("Product pageinfo", pageinfo);
    this.handleFilterQuery();
  }

  handleAdd = () => {
    const {router} = this.props;
    router.push('/product_add');
  }

  handleEdit = (item) => {
    const {router} = this.props;
    router.push('/product/'+item.prodID);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.products;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.products;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.products;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.products;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.products;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus
    }
    actions.getProducts(params);
  }

  handleSelectStatus = (e) => {
    console.log("handleSelectStatus", e);
    let {pageinfo} = this.props.data.products;
    pageinfo.searchFilter = e;
    this.handleFilterQuery();
  }

  handleSelectAll = (e) => {
    console.log("handleSelectAll", e);
    let {pageinfo} = this.props.data.products;
    pageinfo.pageSize = e;
    this.handleFilterQuery();
  }

  render() {
    const {pageinfo} = this.props.data.products;
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Products</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.products.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.products.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.products.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}


        <Card className="order-card">
          <Form>
            <Row>
              <Col md={12} className="inline-block">
                <AddButton
                  handleOpen={this.handleAdd}/>&nbsp;&nbsp;
                {/*<ShowAllButton
                  />&nbsp;&nbsp;
                <ActiveButton
                  />&nbsp;&nbsp;
                <InactiveButton
                  />*/}
                <RaisedButton
                  label="Show All"
                  className="white-btn"
                  onClick={() => {this.handleSelectAll('100')}}
                />&nbsp;&nbsp;
                <RaisedButton
                  label="Active"
                  className="white-btn"
                  icon={<Done className="green-icon" />}
                  onClick={() => {this.handleSelectStatus('active')}}
                />&nbsp;&nbsp;
                <RaisedButton
                  label="Inactive"
                  className="white-btn"
                  icon={<Clear className="red-icon" />}
                  onClick={() => {this.handleSelectStatus('inactive')}}
                />

                <SearchBar
                    pageinfo={data.products.pageinfo}
                    handleChange={this.handleSearchChange}
                    handleClose={this.handleSearchClose}/>
                {/*<Label className="blue-icon pull-right-responsive search"><Search className='nav-icon'/></Label>
                <Input type="text" name="search" placeholder="" className="search-prod"/>*/}

              </Col>
            </Row>
          </Form>
        </Card>

        { /* List */ }
        <ProductList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

export default ProductComponent;
