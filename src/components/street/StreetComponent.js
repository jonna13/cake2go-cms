'use strict';

import React, {
  Component,
} from 'react';
import Divider from 'material-ui/Divider';
import Config from '../../config/base';
import {AddButton, ViewButton, InactiveButton, ActiveButton} from '../common/buttons';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Grid, Row, Col, Glyphicon } from 'react-bootstrap';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import SearchBar from '../common/SearchComponent';
import CheckStatusBar from '../common/CheckStatusComponent';
import PageSizeComponent from '../common/PageSizeComponent';
import ChevronLeft from 'material-ui/svg-icons/navigation/chevron-left';
import FlatButton from 'material-ui/FlatButton';



import StreetList from './StreetList';

class StreetComponent extends React.Component {

  componentWillMount(){
    const {params} = this.props;
    console.log("street params", params);
    let {pageinfo} = this.props.data.streets;
    pageinfo.selectedLocation = params.city;
    this.handleFilterQuery();
  }

  handleAdd = (item) => {
    const {router} = this.props;
    const {params} = this.props;
    console.log("handleAdd street params", params);
    router.push('/street_add/'+ params.city);
  }

  handleEdit = (item) => {
    console.log("handleEdit Component", item);
    const {router} = this.props;
    router.push('/street/'+item.city+'/'+item.street);
  }

  handleStatusChange = (e, val) => {
    let status = (val == 2) ? 'active' : (val == 3) ? 'inactive' : '';
    let {pageinfo} = this.props.data.streets;
    pageinfo.selectedStatus = status;
    this.handleFilterQuery();
  }

  handlePageSize = (e, val) => {
    let {pageinfo} = this.props.data.streets;
    pageinfo.pageSize = val;
    this.handleFilterQuery();
  }

  handleSearchChange = (text) => {
    let {pageinfo} = this.props.data.streets;
    pageinfo.searchFilter = text;
    this.handleFilterQuery();
  }

  handleSearchClose = () => {
    let {pageinfo} = this.props.data.streets;
    pageinfo.searchFilter = '';
    this.handleFilterQuery();
  }

  handleFilterQuery = () => {
    let {actions} = this.props;
    let {pageinfo} = this.props.data.streets;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: pageinfo.sortColumnName,
      sortOrder: pageinfo.sortOrder,
      selectedStatus: pageinfo.selectedStatus,
      selectedLocation: pageinfo.selectedLocation
    }
    console.info('StreetComponent: param', params);
    actions.getStreets(params);
  }
  // handle going back
  handleReturn = () => {
    console.log("handleReturn Component");
    const {router} = this.props;
    router.push('/deliverylocation');
  }

  render() {
    const {pageinfo} = this.props.data.streets;
    console.log("pageinfo", this.props.data.streets);
    const {data} = this.props;
    return (
      <div>
        <h2 className='content-heading'>Delivery Locations - Streets</h2>
        <Divider className='content-divider2' />
        {/*<h5 className='content-record-label'>Records: <b>{ pageinfo.totalRecord }</b></h5>
        <Divider className='content-divider' />*/}

        { /* Action Buttons */ }
        {/*<AddButton
            handleOpen={this.handleAdd}/>
        <CheckStatusBar
            pageinfo={data.streets.pageinfo}
            handleChange={this.handleStatusChange}/>
        <PageSizeComponent
            pageinfo={data.streets.pageinfo}
            handleChange={this.handlePageSize}/>
        <SearchBar
            pageinfo={data.streets.pageinfo}
            handleChange={this.handleSearchChange}
            handleClose={this.handleSearchClose}/>*/}

          <Card className="order-card">
            <Form>
              <Row>
                <Col md={12} className="inline-block">
                  <FlatButton
                    label="Back"
                    className="back-btn"
                    icon={<ChevronLeft className="blue-icon" />} onClick={() => {this.handleReturn()}}/>

                  <AddButton
                    handleOpen={this.handleAdd}/>&nbsp;&nbsp;

                  {/*<SearchBar
                      pageinfo={data.streets.pageinfo}
                      handleChange={this.handleSearchChange}
                      handleClose={this.handleSearchClose}/>*/}
                </Col>
              </Row>
            </Form>
          </Card>

        { /* List */ }
        <StreetList
          data={data}
          actions={this.props.actions}
          onEdit={this.handleEdit} />
      </div>
    );
  }
}

StreetComponent.displayName = 'StreetStreetComponent';

// Uncomment properties you need
// StreetComponent.propTypes = {};
// StreetComponent.defaultProps = {};

export default StreetComponent;
