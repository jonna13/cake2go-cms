/**
 * Created by jonna on 10/6/18.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import {ListDropChips} from '../common/list';


class StreetForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      street: '',
      city: '',
      status: '',
      branchRemove: false
    };
  }

  componentWillMount(){
    const {actions} = this.props;
    actions.getCities();
    // for add
    if (!this.props.shouldEdit){
      this.setState({
        shouldDisplay: true,
      });
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    console.log("componentWillReceiveProps", nextProps);
    if (nextProps.shouldEdit) {
      this.setState({
        city: nextProps.data.selectedRecord.city,
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  // handleStatusChange = (e, idx, status) => {
  //   console.log('status', e.target.value);
  //   this.setState({status: e.target.value});
  //   this.props.onStatusChange(e.target.value);
  // }

  handleCityChange = (e, idx, city) => {
    const {actions} = this.props;
    // let city = this.props.data.cities.filter(x=> { return x.cityID == cityID})[0].city;
    this.setState({city});
    this.props.onCityChange(city);
    console.log("Form handleCityChange",city);
  };

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    let cityList = [];
    const {data} = this.props;
    this.props.params
    console.log('Street Data', data);
    console.log('City Data', this.props.params);

    if (data.cities) {
      cityList.push(<MenuItem key={0} value='' primaryText='Select City' />);
      data.cities.map( (val, idx) => {
        cityList.push(<MenuItem key={(idx+1)} value={val.city} primaryText={val.city} />);
      });
    }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <div>
              <Col className="modal-container">
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">City : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="city" placeholder="" defaultValue={this.props.city}
                      className="fullwidth" readOnly/>
                  </FormGroup>
                  {/*<FormGroup>
                    <DropDownMenu
                      name="city"
                      value={this.state.city}
                      onChange={this.handleCityChange}
                      autoWidth={true}
                      className='form-control-react'>
                      {cityList}
                    </DropDownMenu>
                  </FormGroup>*/}
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Street : </Label>
                </Col>
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <Input type="text" name="street" placeholder="" defaultValue={(data.selectedRecord.street) ? data.selectedRecord.street : '' }
                    onChange={this.props.onChange}  className="fullwidth"/>
                  </FormGroup>
                </Col>
                <Col md={3}>
                  <Label className="label-dft strong-6 title-gotham">Branches : </Label>
                </Col>
                {/*<Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    { (this.props.shouldEdit) ?

                      <Input type="text" name="street" placeholder="" defaultValue={(data.selectedRecord.locID) ? data.selectedRecord.locID : '' }
                      onChange={this.props.onChange}  className="fullwidth"/>:
                      <ListDropChips
                        onChange={this.props.onBranchChange}
                        placeholder="Select Branches"
                        data={this.props.data.branches}
                        remove={this.state.branchRemove}
                        defaultValue={data.selectedRecord.branches}
                        defaultKey="locID"
                        identity="branches"
                      />

                    }

                  </FormGroup>
                </Col>*/}
                <Col md={9}>
                  <FormGroup>
                    <Label hidden></Label>
                    <ListDropChips
                        onChange={this.props.onBranchChange}
                        placeholder="Select Branches"
                        data={this.props.data.branches}
                        remove={this.state.branchRemove}
                        defaultValue={data.selectedRecord.branches}
                        defaultKey="locID"
                        identity="branches"
                      />
                  </FormGroup>
                </Col>
                <Col md={3}>
                </Col>
                <Col md={9}>
                  <Checkbox
                    label="Status"
                    className='chkbx-label'
                    defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                    onCheck={this.onCheck}
                  />
                </Col>
              </Col>
            </div>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default StreetForm;
