/**
 * Created by jonna on 10/6/18.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import GridRow from './StreetGridRow';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

class StreetList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getStreets(params);
  }

  render(){
    let {streets} = this.props.data;
    const headers = [
      { title: '', value: 'dateAdded'},
      { title: 'Status', value: 'status'},
      { title: 'Streets', value: 'street'},
      { title: 'Branch', value: 'locID'}
    ];

    var rows = [];
    if (streets.records.length > 0) {
      streets.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.streets}
            onGetData={this.handleGetData}
            hasImage={false}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.streets}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default StreetList;
