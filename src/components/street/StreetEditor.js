/**
 * Created by jonna on 10/6/18.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton, CreateButton, AddButton, CancelButton} from '../common/buttons';
import Dialog from 'material-ui/Dialog';
import { Grid, Row, Col } from 'react-bootstrap';
import { Button, Form, FormGroup, Label, Input, Container} from 'reactstrap';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import StreetForm from './StreetForm';


class StreetEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      street: {
        street: '',
        status: false,
        city: '',
        branches: ''
      },
      open: true
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewStreet(this.props.params.id);
    }
    actions.getCities();
    actions.getBranches();
  }

  componentWillReceiveProps(nextProps){
    console.log("componentWillReceiveProps Form", nextProps);
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.streetState.selectedRecord)) {
        this.setState({
          street: nextProps.streetState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = (params) => {
    console.log("handleReturn Editor", this.props.params.city);
    this.props.router.push('/street/'+ this.props.params.city);
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.street)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_STREET_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateStreet(this.state.street, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.street)) {
    // if (this.validateBranchCode(this.state.street)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_STREET_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions, params} = this.props;
            actions.addStreet(this.state.street, this.props.router, params.city);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let street = this.state.street;
    street[field] = e.target.value;
    console.log("handleData", street[field]);
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    console.log("validateInput street", data);
    // if (data.city == '') {
    //   dialogActions.openNotification('Oops! No city found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
    //   return false;
    // }
    if (data.street == '') {
      dialogActions.openNotification('Oops! No street found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.branches == '') {
      dialogActions.openNotification('Oops! No branches found.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  handleBranchChange = (val) => {
    this.state.street['branches'] = val.map(v=> {return v.id}).join();
    console.log("handleBranchChange", val, this.state.street['branches']);
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.street['status'] = e;
    console.log('Status', e);
  }

  // handle status select
  // handleStatusChange = (e) => {
  //   this.state.street['status'] = e;
  //   console.log('Status', e);
  // }

  // handle city
  handleCityChange = (e) => {
    this.state.street['city'] = e;
    console.log("handleCityChange e", e);
  }

  //close modal
  handleClose = () => {
    this.setState({open: false});
  };

  render(){
    const {actions} = this.props;
    console.log('City Editor', this.props.params.city);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Street' : 'Create New Street'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <Dialog
          title={ (this.props.params.id) ? 'Edit Street' : 'Create New Street'}
          modal={true}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <StreetForm
            data={this.props.streetState}
            shouldEdit={ (this.props.params.id) ? true : false }
            onChange={this.handleData}
            actions={actions}
            onStatusChange={this.handleStatusChange}
            onCityChange={this.handleCityChange}
            onBranchChange={this.handleBranchChange}
            city={this.props.params.city}
            />
            <br />
            <br />
          <div>
            <Col lg={6} md={6} xs={6} className="btn-right padded-tb btn-lg">
              { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <CreateButton handleOpen={this.handleAdd}/> }
            </Col>
            <Col lg={6} md={6} xs={6} className="btn-left padded-tb btn-lg">
              <CancelButton
                handleOpen={this.handleReturn}
              />
            </Col>
          </div>
        </Dialog>



      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    streetState: state.streetState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    streetAction: require('../../actions/streetAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.streetAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(StreetEditor);
