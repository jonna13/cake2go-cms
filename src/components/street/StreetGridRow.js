import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import AppStyles from '../../styles/Style';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Checkbox from 'material-ui/Checkbox';

class GridRowComponent extends Component {

  // handleStreet = () => {
  //   const {router} = this.props;
  //   router.push('/street/'+item.street);
  // }

  render(){
    let item = this.props.items;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <MoreVertIcon />
          </IconButton>
        </td>
        <td>
          <label className='capital'>{item.status}</label>
        </td>
        <td className="pointer blue-text" onClick={()=> { this.props.onEditClick(item); }}>
          {item.street}
        </td>
        <td className="pointer blue-text" onClick={()=> { this.props.onEditClick(item); }}>
          {item.locID}
        </td>
      </tr>
    );
  }
}

export default GridRowComponent;
