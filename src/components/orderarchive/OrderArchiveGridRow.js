import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import AppStyles from '../../styles/Style';

class OrderArchiveGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr>
        <td>
          {item.fname+' '+item.lname}
        </td>
        <td>
          {item.transactionID}
        </td>
        <td>
          {item.locName}
        </td>
        <td>
          {item.grandTotal}
        </td>
        <td>
          <label className={ (item.paymentStatus == 'Cancelled') ? 'red-text strong-7 capital' : 'green-txt strong-7 capital'}>{item.paymentStatus}</label>
        </td>
      </tr>
    );
  }
}

export default OrderArchiveGridRow;
