/**
 * Created by jonna on 2/02/18.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './OrderArchiveGridRow';

class OrderArchiveList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getArchiveOrders(params);
  }

  render(){
    let {archive} = this.props.data;
    const headers = [
      { title: 'Customer Name', value: 'fname'},
      { title: 'Trans. No.', value: 'transactionID'},
      { title: 'Branch', value: 'locName'},
      { title: 'Total Amount', value: 'grandTotal'},
      { title: 'Status', value: 'paymentStatus'}
    ];

    var rows = [];
    if (archive.records.length > 0) {
      archive.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}
          module={Config.IMAGE.CATEGORY}/>);
      });
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.archive}
            onGetData={this.handleGetData}
            imageModule={Config.IMAGE.CATEGORY}
            hasImage={true}
            imageKey='image'
            onEditClick={this.props.onEdit} />
        :
        <ListTable
          headers={headers}
          data={this.props.data.archive}
          onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default OrderArchiveList;
