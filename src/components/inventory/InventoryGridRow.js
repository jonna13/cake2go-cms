import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Checkbox from 'material-ui/Checkbox';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';
import AppStyles from '../../styles/Style';
import {ProperCase} from '../common/Utility';
import {AddQtyButton} from '../common/buttons';

class InventoryGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.image;

    return(
      <tr className="padding-l-25">
        <td>
        </td>
        <td>
          {item.productCode}
        </td>
        <td>
          {item.categoryName}
        </td>
        <td>
          {item.name}
        </td>
        <td>
          {item.quantity}
        </td>
        <td style={{ width: '15%' }} className="pointer" onClick={()=> { this.props.onEditClick(item); }}>
          <AddQtyButton
            handleOpen={this.handleReturn}
          />
        </td>
      </tr>
    );
  }
}

export default InventoryGridRow;
