/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import TermForm from './TermForm';


class TermEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      term: {
        name: '',
        description: '',
        status: true,
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewTerm(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.termState.selectedRecord)) {
        this.setState({
          term: nextProps.termState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/term');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.term)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_TERM_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateTerm(this.state.term, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.term)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_TERM_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addTerm(this.state.term, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let term = this.state.term;
    term[field] = e.target.value;
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.description == '') {
      dialogActions.openNotification('Oops! No description found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    let newDescription = data.description.replace(/<br\/>/g, "\n");
    this.state.term['description'] = newDescription;
    return true;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.term['status'] = e;
  }


  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Terms and Conditions' : 'Add Terms and Conditions'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <TermForm
          data={this.props.termState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onStatusChange={this.handleStatusChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    termState: state.termState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    termAction: require('../../actions/termAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.termAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(TermEditor);
