/**
 * Created by jonna on 8/31/18.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import {CleanWord} from '../common/Utility';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';
import { Button, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';
import { ControlLabel, FormControl } from 'react-bootstrap';


class CategoryForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleTypeChange = (e, idx, type) => {
    this.setState({type});
    this.props.onTypeChange(type);
  }

  handleStatusChange = (e, idx, status) => {
    console.log('status', e.target.value);
    this.setState({status: e.target.value});
    this.props.onStatusChange(e.target.value);
  }

  // onCheck = (e, val) =>{
  //   this.props.onStatusChange(val);
  // }

  render(){
    const {data} = this.props;
    console.log('CategoryForm', data);

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <div>
                <Col className="modal-container">
                  <Col md={4}>
                    <Label className="label-dft strong-6 title-gotham">Category Name : </Label>
                  </Col>
                  <Col md={8}>
                    <FormGroup>
                      <Label hidden></Label>
                      <Input type="text" name="name" id="name" placeholder="" defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                      onChange={this.props.onChange}  className="fullwidth"/>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <Label className="label-dft strong-6 title-gotham">Status : </Label>
                  </Col>
                  <Col md={8}>
                    <FormGroup>
                      <Input type="select" name="status" id="status" value={(data.selectedRecord.status) ? data.selectedRecord.status : this.state.status} onChange={this.handleStatusChange}>
                        <option value=""> Please Select Status </option>
                        <option value="active"> Active </option>
                        <option value="inactive"> Inactive </option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <Label className="label-dft strong-6 title-gotham">Description : </Label>
                  </Col>
                  <Col md={8}>
                    <FormGroup>
                      <Label hidden></Label>
                      <Input type="textarea" name="description" id="description" placeholder="" defaultValue={CleanWord((data.selectedRecord.description) ? data.selectedRecord.description : '') }
                      onChange={this.props.onChange}  className="fullwidth medium-textarea"/>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <Label className="label-dft strong-6 title-gotham">Image : </Label>
                  </Col>
                  <Col md={8}>
                    <div className='item-image-box'>
                      <ImageUpload image={null}
                        onImageChange={this.props.onImageChange}
                        image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                        imageModule={this.props.imageModule}
                        info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                    </div>
                  </Col>
                {/*<TextField
                  name="name"
                  hintText="Enter Name"
                  floatingLabelText="Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}
                    info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                </div>*/}

                </Col>
            </div>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default CategoryForm;
