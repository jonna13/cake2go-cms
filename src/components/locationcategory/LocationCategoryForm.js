/**
 * Created by jedachas on 3/8/17.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';

class LocationCategoryForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      shouldDisplay: false,
      name: ''
    };
  }

  componentWillMount(){
    const {actions, data} = this.props;
    // actions.getCategory();
    // for add
    if (!this.props.shouldEdit){
      this.setState({
        shouldDisplay: true,
      });
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  onLocFlagCheck = (e, val) => {
    this.props.onLocFlagChange(val);
  }

  render(){
    const {data} = this.props;
    console.log('Location Category Form data', data);

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={12} lg={12}>
              <div className='content-field-holder'>
                <TextField
                  name="name"
                  hintText="Enter Location Category Name"
                  floatingLabelText="Location Category Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default LocationCategoryForm;
