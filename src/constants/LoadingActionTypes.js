/**
 * Created by jedachas on 2/9/17.
 *
 * Constant Loading Action Types
 *
 */

export const SHOW_LOADING = 'SHOW_LOADING';
export const HIDE_LOADING = 'HIDE_LOADING';
