/**
 * Created by jonna on 10/6/18.
 */
import * as types from '../constants/StreetActionTypes';
import update from 'react-addons-update';

const initialState = {
  streets: {
    records: [],
    pageinfo: {
      totalPage: 0,
      sortColumnName: '',
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: '',
      selectedLocation: ''
    },
    category: [],
  },
  streetcategorys: [],
  cities: [],
  selectedRecord: {},
  branches: []
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_STREET_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        streets: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_STREET_SUCCESS:
      console.info('GET_STREET_SUCCESS', action.data);
      return update(state, {
        streets: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

      case types.GET_CATEGORY_SUCCESS:
      console.log('cat data', action.data);
        return update(state, {
          streets: {
            category: {
              $set: action.data.data
            }
          }
        });

      case types.GET_CITY_SUCCESS:
      console.log("GET_CITY_SUCCESS Reducer", action.data.data);
        return update(state, {
          cities: {
            $set: action.data.data
          }
        });

      case types.GET_CITY_FAILED:
        return update(state, {
          cities: {
            records: {
              $set: []
            }
          }
        });

      case types.GET_STREET_CATEGORY_SUCCESS:
        return update(state, {
          streetcategorys: {
            $set: action.data.data
          }
        });

      case types.GET_STREET_CATEGORY_FAILED:
        return update(state, {
          streetcategorys: {
            records: {
              $set: []
            }
          }
        });

      case types.GET_BRANCH_SUCCESS:
        return update(state, {
          branches: { $set: action.data.data }
        });

      case types.GET_BRANCH_FAILED:
        return update(state, {
          branches: { $set: [] }
        });

    default:
      return state;

  }
}
