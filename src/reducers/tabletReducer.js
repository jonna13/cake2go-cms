/**
 * Created by jedachas on 3/9/17.
 */

import * as types from '../constants/TabletActionTypes';
import update from 'react-addons-update';

const initialState = {
  tablets: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  locations: [],
  brands: [],
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_TABLET_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        tablets: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_TABLET_SUCCESS:
      return update(state, {
        tablets: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    case types.GET_TABLETLOCATION_SUCCESS:
      return update(state, {
        locations: {
          $set: action.data.data
        }
      });

    case types.GET_TABLETLOCATION_FAILED:
      return update(state, {
        locations: {
          records: {
            $set: []
          }
        }
      });

    case types.GET_TABLETBRAND_SUCCESS:
      return update(state, {
        brands: {
          $set: action.data.data
        }
      });

    case types.GET_TABLETBRAND_FAILED:
      return update(state, {
        brands: {
          records: {
            $set: []
          }
        }
      });

    default:
      return state;

  }
}
