/**
 * Created by jonna on 10/6/18.
 */
import * as types from '../constants/CityActionTypes';
import update from 'react-addons-update';

const initialState = {
  citys: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: '',
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: ''
    },
    category: [],
    branchCodes: [],

  },
  citycategorys: [],
  selectedRecord: {}
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_CITY_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        citys: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_CITY_SUCCESS:
      console.info('GET_CITY_SUCCESS', action.data);
      return update(state, {
        citys: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

      case types.GET_CATEGORY_SUCCESS:
      console.log('cat data', action.data);
        return update(state, {
          citys: {
            category: {
              $set: action.data.data
            }
          }
        });

      case types.GET_BRANCHCODE_SUCCESS:
        return update(state, {
          branchCodes: {
            $set: action.data.data
          }
        });

      case types.GET_BRANCHCODE_FAILED:
        return update(state, {
          branchCodes: {
            records: {
              $set: []
            }
          }
        });

      case types.GET_CITY_CATEGORY_SUCCESS:
      console.log("GET_CITY_CATEGORY_SUCCESS Reducer", action.data.data);
        return update(state, {
          citycategorys: {
            $set: action.data.data
          }
        });

      case types.GET_CITY_CATEGORY_FAILED:
        return update(state, {
          citycategorys: {
            records: {
              $set: []
            }
          }
        });

    default:
      return state;

  }
}
