/**
 * Created by jonna on 8/28/18.
 */
import * as types from '../constants/AnnouncementActionTypes';
import update from 'react-addons-update';

const initialState = {
  announcements: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: '',
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: ''
    }
  },
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_ANNOUNCEMENT_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        announcements: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_ANNOUNCEMENT_SUCCESS:
      return update(state, {
        announcements: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    default:
      return state;

  }
}
