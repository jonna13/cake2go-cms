/**
 * Created by jonna on 8/29/18.
 */
import React from 'react';
import * as types from '../constants/AnnouncementActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getAnnouncements = (config) => {

  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=announcementtable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      console.log("getAnnouncements", data);
      if (data.response == 'Success') {
        dispatch(loadAnnouncementSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.ANNOUNCEMENTTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.ANNOUNCEMENTTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewAnnouncement = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=announcementtable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewAnnouncementSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.ANNOUNCEMENTTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.ANNOUNCEMENTTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}


export let addAnnouncement = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.ANNOUNCEMENT
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_announcement';
  props.status = (props.status) ? 'active' : 'inactive';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (!_.isEmpty(image)) {

      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          post(props).then(result => {
            let {data} = result;
            console.info('post addAnnouncement', result);
            if (data.response == 'Success') {
              dispatch(dialogAction.openNotification('New announcement added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
              router.push('/announcement');
            }
            else if (data.response == 'Expired'){
              dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
              dispatch(auth.logoutUser());
            }
            else{
              dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
            }
            dispatch(loadingAction.hideLoading());

          }).catch( e => {
            dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
            dispatch(loadingAction.hideLoading());
            console.info('Error', e);
          });
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }

      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });

    }
    else{
      dispatch(loadingAction.hideLoading());
      dispatch(dialogAction.openNotification('No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
    }
  }
}

export let updateAnnouncement = (props, image, router) => {
  let uploadparam = {
    image: image,
    module: Config.IMAGE.ANNOUNCEMENT
  };
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_announcement';
  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }
  return dispatch => {
    dispatch(loadingAction.showLoading());
    if (_.isEmpty(image)){
      updateSelectedRecord(props, router, dispatch);
    }
    else{
      upload(uploadparam).then(result => {
        let {data} = result;
        if (data.response == "Success") {
          props.image = data.data;
          updateSelectedRecord(props, router, dispatch);
        }
        else{
          dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
        }
      }).catch( e => {
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
        dispatch(loadingAction.hideLoading());
        console.info('Error', e);
      });
    }
  }
}

let updateSelectedRecord = (props, router, dispatch) => {
  post(props).then(result => {
    let {data} = result;
    console.info('updateAnnouncement', result);
    if (data.response == 'Success') {
      dispatch(dialogAction.openNotification('Great! Announcement updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
      dispatch(removeSelectedRecord());
      router.push('/announcement');
    }
    else if (data.response == 'Expired'){
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
      dispatch(auth.logoutUser());
    }
    else{
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
    }
    dispatch(loadingAction.hideLoading());
  }).catch( e => {
    dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
    dispatch(loadingAction.hideLoading());
    console.info('Error', e);
  });
}


export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadAnnouncementSuccess = (data) => {
  return {
    type: types.GET_ANNOUNCEMENT_SUCCESS,
    data
  }
}

export let viewAnnouncementSuccess = (data) => {
  return{
    type:types.VIEW_ANNOUNCEMENT_SUCCESS,
    data
  }
}
