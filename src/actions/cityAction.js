/**
 * Created by jonna on 10/6/18.
 */
import React from 'react';
import * as types from '../constants/CityActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getBranchCodes = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=branchCode';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadBranchCodeSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadBranchCodeFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.CITYTABLE));
        dispatch(loadBranchCodeFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getCitys = (config) => {
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=city_streetlookup&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadCitySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.CITYTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.CITYTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewCity = (id) => {
  console.log("viewCity action", id);
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=city&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewCitySuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.CITYTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.CITYTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

// export let addCity = (props, image, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'add_city';
//   props.status = (props.status) ? 'active' : 'inactive';
//   props.locFlag = (props.locFlag) ? 'active' : 'inactive';
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//     dispatch(addUploadCity(dispatch, props, image, router));
//   }
// }
// let addUploadCity = (dispatch, props, image, router) => {
//     let uploadparam = {
//       image: image,
//       module: Config.IMAGE.HOTELS
//     };
//     return dispatch => {
//       if(!_.isEmpty(image)){
//         upload(uploadparam).then(result => {
//           let {data} = result;
//           if (data.response == "Success") {
//             props.image = data.data;
//             dispatch(addCityFull(dispatch, props, image, router));
//           }
//           else{
//             dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
//           }
//         }).catch( e => {
//           dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//           dispatch(loadingAction.hideLoading());
//           console.info('Error', e);
//         });
//       }
//       else{
//         dispatch(loadingAction.hideLoading());
//         dispatch(dialogAction.openNotification('No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//       }
//     }
// }
// let addCityFull = (dispatch, props, image, router) => {
//   return dispatch => {
//     post(props).then(result => {
//       let {data} = result;
//
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('New city added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         router.push('/deliverylocation');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//   }
// }
export let addCity = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_city';
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());

    post(props).then(result => {
      let {data} = result;
      console.log(' City Dataaa', props);

      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('New city added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/deliverylocation');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });

  }
}

// export let updateCity = (props, image, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'update_city';
//
//   if (typeof props.status == 'boolean') {
//     props.status = (props.status) ? 'active' : 'inactive';
//   }
//   if (typeof props.locFlag == 'boolean') {
//     props.locFlag = (props.locFlag) ? 'active' : 'inactive';
//   }
//
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//     dispatch(updateUploadCity(dispatch, props, image, router));
//
//   }
// }
//
// let updateUploadCity = (dispatch, props, image, router) => {
//   let uploadparam = {
//     image: image,
//     module: Config.IMAGE.HOTELS
//   };
//   return dispatch => {
//     if(_.isEmpty(image)){
//       dispatch(updateCityFull(dispatch, props, image, router));
//     }
//     else{
//         upload(uploadparam).then(result => {
//           let {data} = result;
//           if (data.response == "Success") {
//             props.image = data.data;
//             dispatch(updateCityFull(dispatch, props, image, router));
//           }
//           else{
//             dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
//           }
//         }).catch( e => {
//           dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//           dispatch(loadingAction.hideLoading());
//           console.info('Error', e);
//         });
//     }
//   }
// }
//
// let updateCityFull = (dispatch, props, image, router) => {
//   return dispatch => {
//     post(props).then(result => {
//       let {data} = result;
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('Great! City updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         dispatch(removeSelectedRecord());
//         router.push('/branch');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//   }
// }

export let updateCity = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_city';

  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }
  if (typeof props.locFlag == 'boolean') {
    props.locFlag = (props.locFlag) ? 'active' : 'inactive';
  }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! City updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        router.push('/deliverylocation');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadCitySuccess = (data) => {
  return {
    type: types.GET_CITY_SUCCESS,
    data
  }
}

export let viewCitySuccess = (data) => {
  return{
    type:types.VIEW_CITY_SUCCESS,
    data
  }
}

export let viewCategorySuccess = (data) => {
  return{
    type:types.GET_CATEGORY_SUCCESS,
    data
  }
}

export let loadBranchCodeSuccess = (data) => {
  return{
    type: types.GET_BRANCHCODE_SUCCESS,
    data
  }
}

export let loadBranchCodeFailed = () => {
  return{
    type: types.GET_BRANCHCODE_FAILED
  }
}

export let loadCityCategorySuccess = (data) => {
  return{
    type: types.GET_CITY_CATEGORY_SUCCESS,
    data
  }
}

export let loadCityCategoryFailed = () => {
  return{
    type: types.GET_CITY_CATEGORY_FAILED
  }
}
