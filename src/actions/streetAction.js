/**
 * Created by jonna on 10/6/18.
 */
import React from 'react';
import * as types from '../constants/StreetActionTypes';
import {get, post, upload} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getCities = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=cities';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadCitiesSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadCitiesFailed(data));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.STREETTABLE));
        dispatch(loadCitiesFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getBranches = () =>{
  let param = '?function=get_json&category='+Config.GATEWAY.CATEGORY+'&table=locations';

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      console.log("getBranches Data", data);
      if (data.response == 'Success') {
        dispatch(loadBranchSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(loadBranchFailed(data));
        dispatch(auth.logoutUser());
      }
      else if (data.response == 'Empty'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EMPTY_TITLE, Config.ERROR_MESSAGE.EMPTY_DESCRIPTION));
        dispatch(loadBranchFailed(data));
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' '
          + Config.ERROR_CODE.DRINKTABLE));
        dispatch(loadBranchFailed(data));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, e);
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let getStreets = (config) => {
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=streetlookup&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus+
  '&selectedLocation='+config.selectedLocation;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadStreetSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.STREETTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.STREETTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewStreet = (id) => {
  console.log("viewStreet action", id);
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=streettable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(viewStreetSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.STREETTABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.STREETTABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

// export let addStreet = (props, image, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'add_street';
//   props.status = (props.status) ? 'active' : 'inactive';
//   props.locFlag = (props.locFlag) ? 'active' : 'inactive';
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//     dispatch(addUploadStreet(dispatch, props, image, router));
//   }
// }
// let addUploadStreet = (dispatch, props, image, router) => {
//     let uploadparam = {
//       image: image,
//       module: Config.IMAGE.HOTELS
//     };
//     return dispatch => {
//       if(!_.isEmpty(image)){
//         upload(uploadparam).then(result => {
//           let {data} = result;
//           if (data.response == "Success") {
//             props.image = data.data;
//             dispatch(addStreetFull(dispatch, props, image, router));
//           }
//           else{
//             dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
//           }
//         }).catch( e => {
//           dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//           dispatch(loadingAction.hideLoading());
//           console.info('Error', e);
//         });
//       }
//       else{
//         dispatch(loadingAction.hideLoading());
//         dispatch(dialogAction.openNotification('No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//       }
//     }
// }
// let addStreetFull = (dispatch, props, image, router) => {
//   return dispatch => {
//     post(props).then(result => {
//       let {data} = result;
//
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('New street added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         router.push('/street');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//   }
// }
export let addStreet = (props, router, city) => {
  console.log("addStreet props", city);
  console.log("addStreet router", router);
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_street';
  props.city = city;
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());

    post(props).then(result => {
      let {data} = result;
      console.log(' Street Dataaa', props);

      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('New street added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/street/'+props.city);
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });

  }
}

// export let updateStreet = (props, image, router) => {
//   props.category = Config.GATEWAY.CATEGORY;
//   props.function = 'update_street';
//
//   if (typeof props.status == 'boolean') {
//     props.status = (props.status) ? 'active' : 'inactive';
//   }
//   if (typeof props.locFlag == 'boolean') {
//     props.locFlag = (props.locFlag) ? 'active' : 'inactive';
//   }
//
//   return dispatch => {
//     dispatch(loadingAction.showLoading());
//     dispatch(updateUploadStreet(dispatch, props, image, router));
//
//   }
// }
//
// let updateUploadStreet = (dispatch, props, image, router) => {
//   let uploadparam = {
//     image: image,
//     module: Config.IMAGE.HOTELS
//   };
//   return dispatch => {
//     if(_.isEmpty(image)){
//       dispatch(updateStreetFull(dispatch, props, image, router));
//     }
//     else{
//         upload(uploadparam).then(result => {
//           let {data} = result;
//           if (data.response == "Success") {
//             props.image = data.data;
//             dispatch(updateStreetFull(dispatch, props, image, router));
//           }
//           else{
//             dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, data.description));
//           }
//         }).catch( e => {
//           dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//           dispatch(loadingAction.hideLoading());
//           console.info('Error', e);
//         });
//     }
//   }
// }
//
// let updateStreetFull = (dispatch, props, image, router) => {
//   return dispatch => {
//     post(props).then(result => {
//       let {data} = result;
//       if (data.response == 'Success') {
//         dispatch(dialogAction.openNotification('Great! Street updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
//         dispatch(removeSelectedRecord());
//         router.push('/branch');
//       }
//       else if (data.response == 'Expired'){
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
//         dispatch(auth.logoutUser());
//       }
//       else{
//         dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
//       }
//       dispatch(loadingAction.hideLoading());
//     }).catch( e => {
//       dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
//       dispatch(loadingAction.hideLoading());
//       console.info('Error', e);
//     });
//   }
// }

export let updateStreet = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_street';

  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }
  // if (typeof props.locFlag == 'boolean') {
  //   props.locFlag = (props.locFlag) ? 'active' : 'inactive';
  // }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! Street updated.', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        router.push('/street/'+props.city);
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadBranchSuccess = (data) =>{
  return{
    type: types.GET_BRANCH_SUCCESS,
    data
  }
}
export let loadBranchFailed = () =>{
  return{
    type: types.GET_BRANCH_FAILED
  }
}
export let loadStreetSuccess = (data) => {
  return {
    type: types.GET_STREET_SUCCESS,
    data
  }
}

export let viewStreetSuccess = (data) => {
  return{
    type:types.VIEW_STREET_SUCCESS,
    data
  }
}

export let viewCategorySuccess = (data) => {
  return{
    type:types.GET_CATEGORY_SUCCESS,
    data
  }
}

export let loadCitiesSuccess = (data) => {
  return{
    type: types.GET_CITY_SUCCESS,
    data
  }
}

export let loadCitiesFailed = () => {
  return{
    type: types.GET_CITY_FAILED
  }
}

export let loadStreetCategorySuccess = (data) => {
  return{
    type: types.GET_STREET_CATEGORY_SUCCESS,
    data
  }
}

export let loadStreetCategoryFailed = () => {
  return{
    type: types.GET_STREET_CATEGORY_FAILED
  }
}
