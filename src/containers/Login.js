import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Config from '../config/base';
import {AlertDialog} from '../components/common/DialogComponent';
import Loading from '../components/common/LoadingComponent';

import LoginComponent from '../components/auth/LoginComponent';

class Login extends Component {
  render() {
    const {actions} = this.props;
    return (
      <MuiThemeProvider muiTheme={Config.MUITHEME}>
        <div>
          <LoginComponent actions={actions} router={this.props.router}/>

          { /* Dialog  */ }
          <Loading {...this.props.loading}/>
          <AlertDialog {...this.props.alert} dialogActions={this.props.dialogActions} />
        </div>
      </MuiThemeProvider>
    );
  }
}

Login.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = {
    authsession: state.authReducer,
    alert: state.dialogState.alert,
    loading: state.loadingState.loading
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    authAction: require('../actions/authAction.js'),
    dialogActions: require('../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.authAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogActions, dispatch)
   };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
