import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/post/PostComponent';

class Post extends Component {
  render() {
    const {actions, postState, router} = this.props;
    return <Main actions={actions} data={postState} router={router}/>;
  }
}

Post.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { postState: state.postState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { postAction : require('../actions/postAction.js') };
  const actionMap = { actions: bindActionCreators(actions.postAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Post);
