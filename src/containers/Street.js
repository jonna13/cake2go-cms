import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/street/StreetComponent';

class Street extends Component {
  render() {
    const {actions, streetState, router, params } = this.props;
    return <Main actions={actions} data={streetState} router={router} params={params}/>;
  }
}

Street.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { streetState: state.streetState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    streetAction: require('../actions/streetAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.streetAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Street);
