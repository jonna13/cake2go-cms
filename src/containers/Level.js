import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/level/LevelComponent';

class Level extends Component {
  render() {
    const {actions, levelState, router } = this.props;
    return <Main actions={actions} data={levelState} router={router}/>;
  }
}

Level.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { levelState: state.levelState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    levelAction: require('../actions/levelAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.levelAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Level);
