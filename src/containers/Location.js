import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/location/LocationComponent';

class Location extends Component {
  render() {
    const {actions, locationState, router } = this.props;
    return <Main actions={actions} data={locationState} router={router}/>;
  }
}

Location.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { locationState: state.locationState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    locationAction: require('../actions/locationAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.locationAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Location);
