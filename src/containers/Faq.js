import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/faq/FaqComponent';

class Faq extends Component {
  render() {
    const {actions, faqState, router } = this.props;
    return <Main actions={actions} data={faqState} router={router}/>;
  }
}

Faq.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { faqState: state.faqState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    faqAction: require('../actions/faqAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.faqAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Faq);
