import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/push/PushComponent';

class Push extends Component {
  render() {
    const {actions, dialogActions} = this.props;
    return <Main actions={actions} dialogActions={dialogActions}/>;
  }
}

Push.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = {};
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    pushAction: require('../actions/pushAction.js'),
    dialogAction: require('../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.pushAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Push);
