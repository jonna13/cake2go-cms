import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/category/CategoryComponent';

class Category extends Component {
  render() {
    const {actions, categoryState, router} = this.props;
    return <Main actions={actions} data={categoryState} router={router}/>;
  }
}

Category.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { categoryState: state.categoryState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { categoryAction : require('../actions/categoryAction.js') };
  const actionMap = { actions: bindActionCreators(actions.categoryAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Category);
