import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/announcement/AnnouncementComponent';

class Announcement extends Component {
  render() {
    const {actions, announcementState, router} = this.props;
    return <Main actions={actions} data={announcementState} router={router}/>;
  }
}

Announcement.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { announcementState: state.announcementState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { announcementAction : require('../actions/announcementAction.js') };
  const actionMap = { actions: bindActionCreators(actions.announcementAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Announcement);
