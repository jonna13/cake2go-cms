import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/about/AboutComponent';

class About extends Component {
  render() {
    const {actions, aboutState, router } = this.props;
    return <Main actions={actions} data={aboutState} router={router}/>;
  }
}

About.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { aboutState: state.aboutState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    aboutAction: require('../actions/aboutAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.aboutAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(About);
