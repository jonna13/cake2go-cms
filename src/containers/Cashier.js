import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/cashier/CashierComponent';

class Cashier extends Component {
  render() {
    const {actions, cashierState, router} = this.props;
    return <Main actions={actions} data={cashierState} router={router}/>;
  }
}

Cashier.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { cashierState: state.cashierState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { cashierAction : require('../actions/cashierAction.js') };
  const actionMap = { actions: bindActionCreators(actions.cashierAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Cashier);
