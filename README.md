# Introduction
Repository project of Midas CMS - a level tiered project which includes:

- Profile Module

- Settings Module (leveled)

- Level Module

- Push Module

- Product Category Module

- Product Subcategory Module

- Product Module

- Location Module

- Post Module

- Loyalty Module (hidden)

- Voucher Module

- Tablet Module


# How to run
type `npm i`.
then run `npm run serve`.

# How to deploy
type `npm run dist`.