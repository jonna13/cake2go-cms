/**
 * Created by jonna on 2/7/18.
 */
import React from 'react';
import * as types from '../constants/SocialMediaActionTypes';
import {get, post} from '../api/data.api';
import axios from 'axios';
import * as auth from './authAction';
import * as dialogAction from './dialogAction';
import * as loadingAction from './loadingAction';
import Config from '../config/base';
import _ from 'lodash';

export let getSocialMedias = (config) => {
  console.info('SocialMediaAction getSocialMedias');
  let param = '?function=jsonlist&category='+Config.GATEWAY.CATEGORY+'&table=socialmediatable&sortColumnName='+config.sortColumnName+
  '&sortOrder='+config.sortOrder+'&pageSize='+config.pageSize+
  '&currentPage='+config.currentPage+'&searchFilter='+config.searchFilter+
  '&selectedStatus='+config.selectedStatus;

  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(loadSocialMediaSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.SOCIALMEDIATABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.SOCIALMEDIATABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let viewSocialMedia = (id) => {
  let param = '?function=view_record&category='+Config.GATEWAY.CATEGORY+'&table=socialmediatable&filter='+id;
  return dispatch => {
    dispatch(loadingAction.showLoading());
    get(param).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        console.log('viewsocialmedia', data);
        dispatch(viewSocialMediaSuccess(data));
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_FETCH_DESCRIPTION + ' ' + Config.ERROR_CODE.SOCIALMEDIATABLE));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.CONNECTION_TITLE, Config.ERROR_MESSAGE.CONNECTION_DESCRIPTION + ' ' + Config.ERROR_CODE.SOCIALMEDIATABLE));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let addSocialMedia = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'add_socialmedia';
  props.status = (props.status) ? 'active' : 'inactive';
  return dispatch => {
    dispatch(loadingAction.showLoading());

    post(props).then(result => {
      let {data} = result;

      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('New social media added!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        router.push('/socialmedia');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_ADD_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());

    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });

  }
}

export let updateSocialMedia = (props, router) => {
  props.category = Config.GATEWAY.CATEGORY;
  props.function = 'update_socialmedia';

  if (typeof props.status == 'boolean') {
    props.status = (props.status) ? 'active' : 'inactive';
  }

  return dispatch => {
    dispatch(loadingAction.showLoading());
    post(props).then(result => {
      let {data} = result;
      if (data.response == 'Success') {
        dispatch(dialogAction.openNotification('Great! social media updated', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY));
        dispatch(removeSelectedRecord());
        router.push('/socialmedia');
      }
      else if (data.response == 'Expired'){
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.EXPIRED_TITLE, Config.ERROR_MESSAGE.EXPIRED_DESCRIPTION));
        dispatch(auth.logoutUser());
      }
      else{
        dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.FAILED_TITLE, Config.ERROR_MESSAGE.FAILED_UPDATE_DESCRIPTION));
      }
      dispatch(loadingAction.hideLoading());
    }).catch( e => {
      dispatch(dialogAction.openAlert(Config.ERROR_MESSAGE.ERROR_TITLE, Config.ERROR_MESSAGE.ERROR_DESCRIPTION));
      dispatch(loadingAction.hideLoading());
      console.info('Error', e);
    });
  }
}

export let changefilterStatus = (status) => {
  return {
    type: types.CHANGE_FILTER_STATUS,
    status: status
  }
}

export let removeSelectedRecord = () => {
  return {
    type: types.REMOVE_SELECTED_RECORD
  }
}

/*
* Type Handlers
* Success
*/
export let loadSocialMediaSuccess = (data) => {
  return {
    type: types.GET_SOCIALMEDIA_SUCCESS,
    data
  }
}

export let viewSocialMediaSuccess = (data) => {
  return{
    type:types.VIEW_SOCIALMEDIA_SUCCESS,
    data
  }
}
