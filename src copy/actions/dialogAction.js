/**
 * Created by jedachas on 3/1/17.
 *
 *  Dialog Action
 *
 */

import * as types from '../constants/DialogActionTypes';

export const openAlert = (title, message) => {
  return{
    type: types.DIALOG_OPEN_ALERT,
    title,
    message,
    open: true,
  }
}

export const closeAlert = () => {
  return{
    type: types.DIALOG_CLOSE_ALERT
  }
}

export const openConfirm = (title, message, confirmLabel, closeLabel, onClose=null) => {
  return{
    type: types.DIALOG_OPEN_CONFIRM,
    title,
    message,
    confirmLabel,
    closeLabel,
    onClose,
    open: true
  }
}

export const closeConfirm = () => {
  return{
    type: types.DIALOG_CLOSE_CONFIRM,
  }
}

export const openNotification = (message, delay) => {
  return{
    type: types.OPEN_NOTIFICATION,
    message,
    delay
  }
}

export const closeNotification = (message) => {
  return{
    type: types.CLOSE_NOTIFICATION
  }
}
