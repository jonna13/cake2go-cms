import {AUDIT_ACTION} from './const';

module.exports = function(parameter) {
  return { type: AUDIT_ACTION, parameter };
};
