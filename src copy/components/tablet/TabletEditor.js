/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import TabletForm from './TabletForm';


class TabletEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      tablet: {
        locID: '',
        brandID: '',
        brandName: '',
        postype: 'pasi',
        deploy: 'false',
        status: ''
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewTablet(this.props.params.id);
    }
    actions.getBrands();
    actions.getLocations();
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.tabletState.selectedRecord)) {
        this.setState({
          tablet: nextProps.tabletState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/tablet');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.tablet)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_TABLET_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateTablet(this.state.tablet, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.tablet)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_TABLET_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addTablet(this.state.tablet, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let tablet = this.state.tablet;
    tablet[field] = e.target.value;
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.locID == '') {
      dialogActions.openNotification('Oops! No location found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.brandID == '') {
      dialogActions.openNotification('Oops! No brand found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle change in Category
  handleLocationChange = (value) => {
    this.state.tablet['locID'] = value;
  }

  handleBrandChange = (value, name) => {
    this.state.tablet['brandID'] = value;
    this.state.tablet['brandName'] = name;
  }

  handlePosTypeChange = (value) => {
    this.state.tablet['postype'] = value;
  }

  handleDeployChange = (value) => {
    this.state.tablet['deploy'] = value;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.tablet['status'] = e;
  }


  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Tablet' : 'Add Tablet'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <TabletForm
          data={this.props.tabletState}
          actions={this.props.actions}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onLocationChange={this.handleLocationChange}
          onBrandChange={this.handleBrandChange}
          onPosTypeChange={this.handlePosTypeChange}
          onDeployChange={this.handleDeployChange}
          onStatusChange={this.handleStatusChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    tabletState: state.tabletState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    tabletAction: require('../../actions/tabletAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.tabletAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(TabletEditor);
