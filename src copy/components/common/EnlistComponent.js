'use strict';

import React, {Component} from 'react';
import Config from '../../config/base';
import Pagination from './table/PaginationComponent';
import Thumb from './table/ThumbImage';
import _ from 'lodash';


class EnlistComponent extends Component{
  componentWillMount(){
    this.populateData();
  }

  populateData(){
    const { pageinfo } = this.props.data;
    var params = {
      pageSize: pageinfo.pageSize,
      currentPage: pageinfo.currentPage,
      searchFilter: pageinfo.searchFilter,
      sortColumnName: (pageinfo.sortColumnName) ? pageinfo.sortColumnName : '',
      sortOrder: (pageinfo.sortOrder) ? pageinfo.sortOrder: '',
      selectedStatus: (pageinfo.selectedStatus) ? pageinfo.selectedStatus: ''
    }
    this.props.onGetData(params);
  }

  renderImageList(data){
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' +
      this.props.imageModule + '/full/'+ data[this.props.imageKey];
    return(
      <div className='enlist-app-media' key={data.id} onClick={ ()=>{this.props.onEditClick(data)} }>
        <div className='enlist-app-first'>
          <Thumb image={path} />
        </div>
        <div className='enlist-app-second'>
          <div>{data.name}</div>
        </div>
        <div className='enlist-app-third'>
          <label className={ (data.status == 'active' ) ?
          'label-active' : 'label-inactive'}>{data.status}</label>
        </div>
      </div>
    );
  }

  renderList(data){
    return(
      <div className='enlist-app-text' key={data.id} onClick={ ()=>{this.props.onEditClick(data)} }>
        <div className='enlist-app-list'>
          <label>{data.name}</label>
          <label className={(data.status == 'active' ) ? 'label-active' : 'label-inactive'}>
            {data.status}</label>
        </div>
      </div>
    );
  }

  pageChanged = (eventKey, e) => {
    e.preventDefault();
    this.props.data.pageinfo.currentPage = eventKey;
    this.populateData();
  }

  render(){
    if (!_.isEmpty(this.props.data.records)) {
      const {records, pageinfo} = this.props.data;
      return (
        <div>
        {Array.prototype.slice.call(records).map(
          (this.props.hasImage)? this.renderImageList : this.renderList, this)}
            {/*{this.renderImageList(records[1])}*/}
          <br/>
          <Pagination Size={pageinfo.totalPage}
            onPageChanged={this.pageChanged}
            currentPage={pageinfo.currentPage}/>
        </div>
      );
    }
    else{
      return null;
    }

  }
}

export default EnlistComponent;
