import React from 'react';
let defaultImage = require('../../../images/no-image.jpg');

class ThumbImage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      hasError: false
    }
  }

  componentWillReceiveProps(){
    this.setState({hasError:false});
  }

  handleError = () => {
    this.setState({hasError:true});
  }

  render(){
    let {image} = this.props;

    if (this.state.hasError) {
      return(
        <img src={defaultImage} />
      );
    }
    else{
      return(
        <img src={image} onError={this.handleError} />
      );
    }
  }
}

export default ThumbImage;
