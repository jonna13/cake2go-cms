'use strict';

import React from 'react';

import IconButton from 'material-ui/IconButton';
import Return from 'material-ui/svg-icons/content/clear';

class ReturnButton extends React.Component{
  render(){
    return(
      <div>
        <div className='v-floating-return-circle hidden-xs'>
          <IconButton tooltip="Click to return" touch={true} tooltipPosition="bottom-left">
            <Return className='floating-return-button-icon' onTouchTap={this.props.handleOpen}/>
          </IconButton>
        </div>

        <div className='floating-return-button visible-xs'>
          <IconButton touch={true} tooltipPosition="bottom-left">
            <Return className='floating-return-button-icon' onTouchTap={this.props.handleOpen}/>
          </IconButton>
        </div>
      </div>
    );
  }
}

export default ReturnButton;
