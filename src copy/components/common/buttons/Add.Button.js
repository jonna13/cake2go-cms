'use strict';

import React from 'react';

import IconButton from 'material-ui/IconButton';
import Add from 'material-ui/svg-icons/content/add';

class AddButton extends React.Component{
  render(){
    return(
      <div>
        <div className='v-floating-action-circle hidden-xs'>
          <IconButton tooltip="Click to add" touch={true} tooltipPosition="bottom-left">
            <Add className='floating-button-icon' onTouchTap={this.props.handleOpen}/>
          </IconButton>
        </div>

        <div className='floating-button visible-xs'>
          <IconButton touch={true} tooltipPosition="bottom-left">
            <Add className='floating-button-icon' onTouchTap={this.props.handleOpen}/>
          </IconButton>
        </div>
      </div>
    );
  }
}

export default AddButton;
