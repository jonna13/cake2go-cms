import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/content/create';
import AppStyles from '../../styles/Style';
import Config from '../../config/base';
import ThumbImage from '../common/table/ThumbImage';

class SettingGridRow extends Component {
  render(){
    let item = this.props.items;
    let path = Config.PROJECT_PATH + '' + Config.IMAGE.PATH +  '' + this.props.module + '/thumb/'+ item.icon;

    return(
      <tr>
        <td>
          <IconButton tooltip="Click to edit item"
            touch={true}
            tooltipPosition="bottom-left"
            iconStyle={AppStyles.editIcon}
            onClick={()=> { this.props.onEditClick(item); }}>
              <EditIcon />
          </IconButton>
        </td>
        <td>
          <div>{item.name}</div>
        </td>
        <td>{item.camount}</td>
        <td>{item.cpoints}</td>
        <td>
          <label className={ (item.status == 'active' ) ? 'label-active' : 'label-inactive'}>{item.status}</label>
        </td>
      </tr>
    );
  }
}

export default SettingGridRow;
