/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';

import VoucherForm from './VoucherForm';

class VoucherEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      voucher: {
        name: '',
        description: '',
        terms: '',
        type: '',
        startDate: '',
        endDate: '',
        cardType: '',
        cardVersion: '',
        frequencyType: '',
        frequencyStart: '',
        frequencyEnd: '',
        parameterString: '',
        parameterValue: '',
        quantity: 0,
        startDate: '',
        endDate: '',
        startTime: '',
        endTime: '',
        redemptionlimit: '',
        action: 'copy',
        image: ''
      },
      uploadImage: {},
      imageModule: Config.IMAGE.VOUCHER,
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewVoucher(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.voucherState.selectedRecord)) {
        this.setState({
          voucher: nextProps.voucherState.selectedRecord
        });
      }
    }
  }

  // handle going back to Voucher
  handleReturn = () => {
    this.props.router.push('/voucher');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    if (this.validateInput(this.state.voucher)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_VOUCHER_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateVoucher(this.state.voucher, this.state.uploadImage, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.voucher)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_VOUCHER_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addVoucher(this.state.voucher, this.state.uploadImage, this.props.router);
          }
      });
    }
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (this.state.voucher.image == '') {
        if (_.isEmpty(this.state.uploadImage)) {
          dialogActions.openNotification('Oops! No image found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
          return false;
        }
    }

    return true;
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let voucher = this.state.voucher;
    voucher[field] = e.target.value;
  }

  // handle image change
  handleImageChange = (e) => {
    this.setState({ uploadImage: e });
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.voucher['status'] = e;
  }

  // handle change in type
  handleTypeChange = (value) => {
    this.state.voucher['type'] = value;
    this.state.voucher['action'] = (value == 'discount' && value == 'flash') ? 'copy' : 'move';
    this.state.voucher['shouldmultiple'] = (value == 'flash') ? 'true' : 'false';
    this.state.voucher['quantity'] = (value == 'flash') ? '1' : '0';
    this.state.voucher['redemptionlimit'] = (value == 'flash') ? '1' : '0';
  }

  handleCardTypeChange = (value) => {
    this.state.voucher['cardType'] = value;
  }

  handleCardVersionChange = (value) => {
    this.state.voucher['cardVersion'] = value;
  }

  handleFrequencyTypeChange = (value) => {
    this.state.voucher['frequencyType'] = value;
  }

  handleFrequencyStartChange = (value) => {
    this.state.voucher['frequencyStart'] = value;
  }

  handleFrequencyEndChange = (value) => {
    this.state.voucher['frequencyEnd'] = value;
  }

  handleTimeStartChange = (value) => {
    this.state.voucher['startTime'] = value;
  }

  handleTimeEndChange = (value) => {
    this.state.voucher['endTime'] = value;
  }

  handleParameterStringChange = (value) => {
    this.state.voucher['parameterString'] = value;
  }

  handleBirthdayChange = (value) => {
    let date = new Date();
    let startDate = date.getFullYear() + '-' + (value + 1) + '-1';
    let endDate = date.getFullYear() + '-' + (value + 1) + '-' + new Date(date.getFullYear(), value + 1, 0).getDate();
    this.state.voucher['startDate'] = startDate;
    this.state.voucher['endDate'] = endDate;
  }

  handleStartDate = (e, date) => {
    let newdate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    this.state.voucher['startDate'] = newdate;
  }

  handleEndDate = (e, date) => {
    let newdate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    this.state.voucher['endDate'] = newdate;
  }

  handleActionChange = (value) => {
    this.state.voucher['action'] = value;
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Voucher' : 'Add Voucher'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <VoucherForm
          data={this.props.voucherState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onStatusChange={this.handleStatusChange}
          onImageChange={this.handleImageChange}
          onTypeChange={this.handleTypeChange}
          onCardTypeChange={this.handleCardTypeChange}
          onCardVersionChange={this.handleCardVersionChange}
          onFrequencyTypeChange={this.handleFrequencyTypeChange}
          onFrequencyStartChange={this.handleFrequencyStartChange}
          onFrequencyEndChange={this.handleFrequencyEndChange}
          onTimeStartChange={this.handleTimeStartChange}
          onTimeEndChange={this.handleTimeEndChange}
          onParameterStringChange={this.handleParameterStringChange}
          onBdayChange={this.handleBirthdayChange}
          onStartDate={this.handleStartDate}
          onEndDate={this.handleEndDate}
          onActionChange={this.handleActionChange}
          imageModule={this.state.imageModule}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    voucherState: state.voucherState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    voucherAction: require('../../actions/voucherAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.voucherAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(VoucherEditor);
