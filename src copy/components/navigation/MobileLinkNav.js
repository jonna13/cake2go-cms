'use strict';

import React, {
  Component,
  PropTypes
} from 'react';
import { Link } from 'react-router';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import classNames from 'classnames';
import Config from '../../config/base';
import jwtDecode from 'jwt-decode';
import InfoComponent from '../common/InfoComponent';
import Header from './HeaderComponent';


class MobileLinkNav extends React.Component {

  state = {
    activeKey: 1,
    open: false,
  }

  handleSelect = (selectedKey) => {
    this.setState({activeKey: selectedKey});

    switch (selectedKey) {
      case 1:
        this.props.router.push('/');
        break;
      // case 2:
      //   this.props.router.push('/level');
      //   break;
      case 2:
        this.props.router.push('/loyalty');
        break;
      case 3:
        this.props.router.push('/product');
        break;
      case 4:
        this.props.router.push('/voucher');
        break;
      // case 4:
      //   this.props.router.push('/brand');
      //   break;
      case 5:
        this.props.router.push('/post');
        break;
      // case 6:
      //   this.props.router.push('/term');
      //   break;
      // case 3.1:
      //   this.props.router.push('/category');
      //   break;
      // case 3.2:
      //   this.props.router.push('/subcategory');
      //   break;
      case 6:
        this.props.router.push('/location');
        break;
      case 7:
        this.props.router.push('/tablet');
        break;
      case 8:
        this.props.router.push('/sku');
        break;
      case 9:
        this.props.router.push('/setting/EARNEM161556lEhA');
        break;
      // case 12:
      //   this.props.router.push('/cashier');
      //   break;
      case 10:
        this.props.router.push('/push');
        break;
      case 11:
        this.props.router.push('/account');
        break;

      default:
        return;

    }
  }

  handleRouteReload = (pathname) => {
    if (pathname == '/loyalty') {
      this.setState({activeKey: 2});
    }
    // if (pathname == '/level') {
    //   this.setState({activeKey: 2});
    // }
    else if (pathname.includes('/product')) {
      this.setState({activeKey: 3});
    }
    else if (pathname.includes('/voucher')) {
      this.setState({activeKey: 4});
    }
    // else if (pathname.includes('/brand')) {
    //   this.setState({activeKey: 4});
    // }
    else if (pathname.includes('/post')) {
      this.setState({activeKey: 5});
    }
    // else if (pathname.includes('/term')) {
    //   this.setState({activeKey: 6});
    // }
    // else if (pathname.includes('/category')) {
    //   this.setState({activeKey: 3.1});
    // }
    // else if (pathname.includes('/subcategory')) {
    //   this.setState({activeKey: 3.2});
    // }
    else if (pathname.includes('/location')) {
      this.setState({activeKey: 6});
    }
    else if (pathname.includes('/tablet')) {
      this.setState({activeKey: 7});
    }
    else if (pathname.includes('/sku')) {
      this.setState({activeKey: 8});
    }
    else if (pathname.includes('/setting/EARNEM161556lEhA')) {
      this.setState({activeKey: 9});
    }
    else if (pathname.includes('/push')) {
      this.setState({activeKey: 10});
    }
    // else if (pathname.includes('/cashier')) {
    //   this.setState({activeKey: 12});
    // }
    else if (pathname.includes('/account')) {
      this.setState({activeKey: 11});
    }
    else if (pathname == '/') {
      this.setState({activeKey: 1});
    }
  }

  componentWillMount(){
    let {pathname} = this.props.router.location;
    this.handleRouteReload(pathname);
  }

  handleClick = (event) => {
    const {router} = this.props;
    const {actions} = this.props;
    actions.logoutUser(this.props.router);
  }

  handleInfo = (event) => {
    this.setState({open: true});
  }

  handleClose = () => {
    this.setState({open:false});
  }

  handleAccount = () => {
    const {router} = this.props;
    router.push('/credential');
  }

  render() {
    const role = jwtDecode(sessionStorage.getItem(Config.MERCHANT_NAME)).role;
    let superUserOnly = classNames({
      'hide': !Config.SUPERUSER.includes(role),
      'visible': Config.SUPERUSER.includes(role)
    });

    return (
      <div className="visible-xs">
        <Header router={this.props.router} onInfo={this.handleInfo} onAccount={this.handleAccount}/>
        <InfoComponent
          open={this.state.open}
          onRequestClose={this.handleClose}/>
      </div>
    )
  }
}


export default MobileLinkNav;
