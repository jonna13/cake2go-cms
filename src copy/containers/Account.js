import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/account/AccountComponent';

class Account extends Component {
  render() {
    const {actions, accountState, router} = this.props;
    return <Main actions={actions} data={accountState} router={router}/>;
  }
}

Account.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { accountState: state.accountState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = { accountAction : require('../actions/accountAction.js') };
  const actionMap = { actions: bindActionCreators(actions.accountAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Account);
