var reducer = require('../../src/reducers/profileReducer');

describe('profileReducer', () => {

  it('should not change the passed state', (done) => {

    const state = Object.freeze({});
    reducer(state, {type: 'INVALID'});

    done();
  });
});
